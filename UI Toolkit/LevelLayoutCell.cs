﻿namespace Cardstone
{
	[System.Serializable]
	public class LevelLayoutCell
	{
		#region Properties

		public LevelTileType LevelTileType { get => levelTileType; set => levelTileType = value; }

		#endregion

		#region Fields

		private LevelTileType levelTileType;

		#endregion

		#region Constructor

		public LevelLayoutCell()
		{
			levelTileType = LevelTileType.Unoccupied;
		}

		#endregion
	}
}