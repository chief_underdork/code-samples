﻿using UnityEngine;

namespace Cardstone 
{
	[CreateAssetMenu(fileName = "NewLevelLayout", menuName = "Levels/Level Layout", order = 3)]
	public class LevelLayout : ScriptableObject
	{
		#region Fields

		[SerializeField, HideInInspector] private LevelLayoutCell[] cells;

		#endregion

		#region Constants

		public const int LayoutSideSize = 5;

		#endregion

		#region Get

		public LevelLayoutCell[] GetCells()
		{
			return cells.IsNullOrEmpty() ? cells = new LevelLayoutCell[LayoutSideSize * LayoutSideSize] : cells;
		}

		#endregion
	}
}