﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Cardstone
{
	[CustomEditor(typeof(LevelLayout))]
	public class LevelLayoutEditor : Editor
	{
		#region Fields

		private VisualElement rootElement;
		private VisualTreeAsset visualTreeAsset;
		private LevelLayout levelLayout;

		private Image[] cellImages;
		private Image[] cellHighlightImages;

		private readonly List<Image> selectedCellImages = new List<Image>();
		
		#endregion

		#region Constants

		private const string VisualTreeAssetAddress = "Assets/Scripts/Editor/UXML/LevelLayoutTemplate.uxml";
		private const string StyleSheetAddress = "Assets/Scripts/Editor/USS/LevelLayoutStyleSheet.uss";
		
		private static readonly Color UnoccupiedCellColor = Color.gray;
		private static readonly Color TrapCellColor = new Color32(102, 90, 90, 255);
		private static readonly Color MonsterCellColor = new Color32(193, 27, 52, 255);
		private static readonly Color LootCellColor = new Color32(190, 132, 22, 255);
		private static readonly Color HighlightColor = new Color32(38, 207, 126, 255);

		#endregion
		
		#region Init

		private void OnEnable()
		{
			rootElement = new VisualElement();
			visualTreeAsset = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(VisualTreeAssetAddress);
			var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(StyleSheetAddress);
			rootElement.styleSheets.Add(styleSheet);

			levelLayout = (LevelLayout)serializedObject.targetObject;
		}

		#endregion

		#region Inspector

		public override VisualElement CreateInspectorGUI()
		{
			visualTreeAsset.CloneTree(rootElement);

			cellHighlightImages = rootElement.Query<Image>(className:"cell-highlight").Build().ToList().ToArray();
			cellImages = new Image[cellHighlightImages.Length]; 
			Texture cellTexture = AssetDatabase.LoadAssetAtPath<Texture>("Assets/Textures/UI/RoundedCorners20Filled.png");
			
			LevelLayoutCell[] layoutCells = levelLayout.GetCells();
			
			for (var i = 0; i < layoutCells.Length; i++)
			{
				Image cellHighlightImage = cellHighlightImages[i];
				cellHighlightImage.image = cellTexture;
				Color highlightColor = HighlightColor;
				highlightColor.a = 0f;
				cellHighlightImage.tintColor = highlightColor;

				Image cellImage = cellHighlightImage.Q<Image>(className: "cell");
				cellImage.image = cellTexture;
				cellImages[i] = cellImage;
				
				SetUpCellImageCallbacks(cellImage);
				
				SetCellImageColor(cellImage, layoutCells[i]);
			}

			IMGUIContainer container = new IMGUIContainer();
			container.onGUIHandler += HandleKeyboardEvents;
			
			rootElement.Add(container);
			
			return rootElement;
		}

		private void HandleKeyboardEvents()
		{
			if (Event.current == null || Event.current.type != EventType.KeyDown)
			{
				return;
			}

			if (selectedCellImages.Count == 0)
			{
				return;
			}
			
			foreach (Image selectedCellImage in selectedCellImages)
			{
				int cellIndex = GetCellImageIndex(selectedCellImage);

				LevelLayoutCell levelLayoutCell = levelLayout.GetCells()[cellIndex];
				
				switch (Event.current.keyCode)
				{
					case KeyCode.C:
						levelLayoutCell.LevelTileType = LevelTileType.Unoccupied;
						break;
					case KeyCode.M:
						levelLayoutCell.LevelTileType = LevelTileType.Monster;
						break;
					case KeyCode.L:
						levelLayoutCell.LevelTileType = LevelTileType.Loot;
						break;
					case KeyCode.T:
						levelLayoutCell.LevelTileType = LevelTileType.Trap;
						break;
					default:
						return;
				}
				
				SetCellImageColor(selectedCellImage, levelLayoutCell);
				DisableHighlight(selectedCellImage);
			}
			
			selectedCellImages.Clear();
			serializedObject.Update();
			serializedObject.ApplyModifiedProperties();

			EditorUtility.SetDirty(levelLayout);
			Repaint();
		}

		#endregion

		#region Images

		private int GetCellImageIndex(Image cellImage)
		{
			VisualElement rowElement = cellImage.parent.parent.parent.parent;
			VisualElement rowHolderElement = rowElement.parent;
			int rowIndex = rowHolderElement.IndexOf(rowElement);

			VisualElement cellHolder = cellImage.parent.parent.parent;
			VisualElement cellElement = cellImage.parent.parent;

			int cellIndex = cellHolder.IndexOf(cellElement);
			
			return rowIndex * LevelLayout.LayoutSideSize + cellIndex;
		}
		
		private void SetCellImageColor(Image cellImage, LevelLayoutCell levelLayoutCell)
		{
			switch (levelLayoutCell.LevelTileType)
			{
				case LevelTileType.Unoccupied:
					cellImage.tintColor = UnoccupiedCellColor;
					break;
				case LevelTileType.Trap:
					cellImage.tintColor = TrapCellColor;
					break;
				case LevelTileType.Monster:
					cellImage.tintColor = MonsterCellColor;
					break;
				case LevelTileType.Loot:
					cellImage.tintColor = LootCellColor;
					break;
				default:
					cellImage.tintColor = UnoccupiedCellColor;
					break;
			}
		}

		private void SetUpCellImageCallbacks(Image cellImage)
		{
			cellImage.RegisterCallback<MouseDownEvent>(callback =>
			{
				if (callback.target != cellImage)
				{
					return;
				}
				
				bool imageIsCurrentlySelected = selectedCellImages.Contains(cellImage);
				bool shiftIsHeldDown = Event.current.shift;

				if (!shiftIsHeldDown)
				{
					foreach (Image selectedCellImage in selectedCellImages)
					{
						DisableHighlight(selectedCellImage);
					}	
					
					selectedCellImages.Clear();
				}

				if (imageIsCurrentlySelected)
				{
					DisableHighlight(cellImage);
					selectedCellImages.Remove(cellImage);
				}
				else
				{
					EnableHighlight(cellImage);
					selectedCellImages.Add(cellImage);
				}
			});
		}

		private void EnableHighlight(Image cellImage)
		{
			SetHighlightColor(cellImage, HighlightColor);	
		}

		private void DisableHighlight(Image cellImage)
		{
			Color highlightColor = HighlightColor;
			highlightColor.a = 0f;
			SetHighlightColor(cellImage, highlightColor);
		}

		private void SetHighlightColor(Image cellImage, Color color)
		{
			((Image) cellImage.parent).tintColor = color;
		}

		#endregion
	}
}