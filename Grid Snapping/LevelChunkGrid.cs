﻿using Sirenix.OdinInspector;
using Unidork.Attributes;
using Unidork.Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Stores data about a grid that handles positioning of objects on a <see cref="LevelChunk"/>.
    /// </summary>
    [ExecuteAlways]
    public class LevelChunkGrid : MonoBehaviour
    {
        #region Fields

	    /// <summary>
	    /// Transform that serves as a holder for the grid.
	    /// </summary>
	    [Space, SettingsHeader, Space]
	    [Tooltip("Transform that serves as a holder for the grid.")]
	    [SerializeField]
	    private Transform gridHolder = null;
	    
        /// <summary>
        /// Component that stores data and handles operations with level chunks.level
        /// </summary>
        [Space, ComponentsHeader, Space] 
        [Tooltip("Component that stores data and handles operations with level chunks.")]
        [SerializeField]
        private LevelChunk levelChunk = null;
	    
	    /// <summary>
	    /// Scriptable object that stores settings for the <see cref="LevelTileGrid"/>.
	    /// </summary>
	    [Space, AssetsHeader, Space]
	    [Tooltip("Scriptable object that stores settings for the level tile grid.")]
	    [SerializeField]
	    private GridSettings gridSettings = null;

	    /// <summary>
	    /// Cells that constitute the level chunk grid.
	    /// </summary>
	    [Tooltip("Cells that constitute the level chunk grid.")]
	    [SerializeField]
	    private LevelChunkGridCell[] cells;

        #endregion
        
#if UNITY_EDITOR

	    #region Editor

	    [Button("Generate Grid")]
	    public void GenerateGrid()
	    {
		    DestroyCellTransforms();
		    
		    int xSize = GridSettings.GridSizeX;
		    int ySize = gridSettings.GetChunkSizeByChunkSizeType(levelChunk.Size);
		    
		    const float centralLaneX = 0f;
		    
		    float rightLaneX = gridSettings.SideLaneXOffsetFromCenter;
		    float leftLaneX = -rightLaneX;

		    const float minZ = 0f;
		    const float gridY = 0f;

		    float distanceBetweenCellRows = gridSettings.DistanceBetweenLevelTileGridRows;
		    
		    cells = new LevelChunkGridCell[xSize * ySize];

		    for (var y = 0; y < ySize; y++)
		    {
			    var xPosition = 0f;
			    
			    for (var x = 0; x < xSize; x++)
			    {
				    switch ((x + 1) % 3)
				    {
					    case 0:
						    xPosition = rightLaneX;
						    break;
					    case 1:
						    xPosition = leftLaneX;
						    break;	
					    case 2:
						    xPosition = centralLaneX;
						    break;
				    }
				    
				    var cellPosition = new Vector3(xPosition, gridY, minZ + y * distanceBetweenCellRows);
				    AddCell(x, y, xSize, cellPosition);
			    }
		    }
	    }

	    private void DestroyCellTransforms()
	    {
		    while (gridHolder.childCount > 0)
		    {
			    DestroyImmediate(gridHolder.GetChild(0).gameObject);
		    }
	    }

	    private void AddCell(int x, int y, int xSize, Vector3 cellPosition)
	    {
		    Transform cellTransform = new GameObject().transform;
		    cellTransform.name = $"Cell {x + 1} {y + 1}";

		    cellTransform.position = cellPosition;
		    cellTransform.SetParent(gridHolder);
			    
		    cells[x + y * xSize] = new LevelChunkGridCell(cellTransform, cellPosition);
	    }

	    private void OnDrawGizmos()
	    {
		    if (!gridSettings.ShowLevelChunkGrid || cells.IsNullOrEmpty())
		    {
			    return;
		    }

		    Color oldColor = Gizmos.color;

		    var unoccupiedCellColor = Color.green;
		    var occupiedCellColor = Color.red;

		    unoccupiedCellColor.a = occupiedCellColor.a = 0.5f;
		    
		    Vector3 gizmoSize = Vector3.one * 0.4f;
		    
		    foreach (LevelChunkGridCell cell in cells)
		    {
			    Gizmos.color = cell.IsOccupied ? occupiedCellColor : unoccupiedCellColor;
			    Gizmos.DrawCube(cell.Position, gizmoSize);
		    }

		    Gizmos.color = oldColor;
	    }

	    private void OnSceneGUI(SceneView sceneView)
	    {
		    Event currentEvent = Event.current;
		    
		    if (currentEvent.type != EventType.KeyDown || currentEvent.keyCode != KeyCode.S)
		    {
			    return;
		    }
		    
		    Camera sceneViewCamera = sceneView.camera;
		    
		    Vector3 mousePosition = Event.current.mousePosition;

		    Vector3 mouseViewportPosition = sceneViewCamera.ScreenToViewportPoint(mousePosition);
		    mouseViewportPosition.y = Mathf.Clamp01(1f - mouseViewportPosition.y);

		    GameObject activeGameObject = Selection.activeGameObject;
		    
		    if (activeGameObject == null)
		    {
			    return;
		    }

		    var snappableObject = activeGameObject.GetComponentInParent<ISnappableLevelChunkObject>();

		    if (snappableObject == null)
		    {
			    return;
		    }

		    float minDistance = float.MaxValue;
		    LevelChunkGridCell closestCell = null;
		    
		    foreach (LevelChunkGridCell cell in cells)
		    {
			    Vector3 cellViewportPosition = sceneViewCamera.WorldToViewportPoint(cell.Position);
			    cellViewportPosition.z = 0f;

			    float distanceToCell = Vector3.Distance(mouseViewportPosition, cellViewportPosition);
			    
			    if (distanceToCell < minDistance)
			    {
				    minDistance = distanceToCell;
				    closestCell = cell;
			    }
		    }

		    snappableObject.RootTransform.position = closestCell.Position;
	    }

	    private void OnEnable()
	    {
		    SceneView.duringSceneGui += OnSceneGUI;
	    }

	    private void OnDestroy()
	    {
		    SceneView.duringSceneGui -= OnSceneGUI;
	    }

	    #endregion

#endif
    }
}