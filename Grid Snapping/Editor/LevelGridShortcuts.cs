﻿using UnityEditor;
using UnityEditor.ShortcutManagement;

namespace AwesomeRunner 
{
    public static class LevelGridShortcuts
    {
        #region Shortcuts

        [Shortcut("Awesome Runner/Grid/Toggle Level Tile Grid")]
        public static void ToggleLevelTileGrid()
        {
            GridSettings gridSettings = GetLevelTileGridSettings();

            gridSettings.ShowLevelTileGrid = !gridSettings.ShowLevelTileGrid;
        }

        [Shortcut("Awesome Runner/Grid/Toggle Level Chunk Grid")]
        public static void ToggleLevelChunkGrid()
        {
            GridSettings gridSettings = GetLevelTileGridSettings();

            gridSettings.ShowLevelChunkGrid = !gridSettings.ShowLevelChunkGrid;
        }
        
        [Shortcut("Awesome Runner/Grid/Toggle Level Chunk Grid Snapping")]
        public static void ToggleLevelChunkGridSnapping()
        {
            GridSettings gridSettings = GetLevelTileGridSettings();

            gridSettings.EnableLevelChunkGridSnapping = !gridSettings.EnableLevelChunkGridSnapping;
        }

        private static GridSettings GetLevelTileGridSettings()
        {
            string leveTileGridSettingsGuid = AssetDatabase.FindAssets("Grid Settings")[0];
            string levelTileGridSettingsPath = AssetDatabase.GUIDToAssetPath(leveTileGridSettingsGuid);

            var levelTileGridSettings = AssetDatabase.LoadAssetAtPath<GridSettings>(levelTileGridSettingsPath);

            return levelTileGridSettings;
        }

        #endregion
    }
}


