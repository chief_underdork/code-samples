﻿using Sirenix.OdinInspector;
using Unidork.Attributes;
#if UNITY_EDITOR
using UnityEditor.Experimental.SceneManagement;
#endif
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Scriptable object that stores settings for the <see cref="LevelTileGrid" /> and the <see cref="LevelChunkGrid" />.
    /// </summary>
    [CreateAssetMenu(fileName = "Grid Settings", menuName = "Level/Grid Settings", order = 1)]
	public class GridSettings : ScriptableObject
	{
		#region Properties

        /// <summary>
		/// Size of the level tile grids along the grid's y axis.
        /// </summary>
        public int GridSizeY => levelChunkSizeData.GetChunkDataByChunkSize(LevelChunkSize.Huge).ChunkSize;

        /// <summary>
		/// Offset along the x axis to place points on the grid that represent side lanes.
        /// </summary>
        /// <value>
		/// Gets the value of the float field sideLaneXOffsetFromCenter.
        /// </value>
        public float SideLaneXOffsetFromCenter => sideLaneXOffsetFromCenter;

		/// <summary>
		/// Distance between level tile grid rows.
		/// </summary>
		/// <value>
		/// Gets the value of the float field distanceBetweenLevelTileGridRows.
		/// </value>
		public float DistanceBetweenLevelTileGridRows  => distanceBetweenLevelTileGridRows;

		/// <summary>
		/// Width of the level tile section.
		/// </summary>
		/// <value>
		/// Gets the value of the float field levelTileSectionWidth.
		/// </value>
		public float LevelTileSectionWidth => levelTileSectionWidth;

#if UNITY_EDITOR

        /// <summary>
        /// Should the level tile grid be shown in the editor?
        /// </summary>
        /// <value>
        /// Gets and sets the value of the Boolean field showLevelTileGrid.
        /// </value>
        public bool ShowLevelTileGrid
		{
			get => showLevelTileGrid;
			set => showLevelTileGrid = value;
		}

        /// <summary>
        /// Should the level tile chunk be shown in the editor?
        /// </summary>
        /// <value>
        /// Gets and sets the value of the Boolean field showLevelChunkGrid.
        /// </value>
        public bool ShowLevelChunkGrid
		{
			get => showLevelChunkGrid;
			set => showLevelChunkGrid = value;
		}

        /// <summary>
        /// Size of level chunks to show in the level tile grid in the editor.
        /// </summary>
        /// <value>
        /// Gets the value of the field chunkSizeToShow.
        /// </value>
        public LevelChunkSize ChunkSizeToShow => chunkSizeToShow;

		/// <summary>
		/// When True, enables snapping of objects on the level chunk grid.
		/// </summary>
		/// <value>
		/// Gets and sets the value of the boolean field enableLevelChunkGridSnapping.
		/// </value>
		public bool EnableLevelChunkGridSnapping { get => enableLevelChunkGridSnapping; set => enableLevelChunkGridSnapping = value; }
#endif

		#endregion

		#region Fields

		/// <summary>
		/// Width of the level tile section.
		/// </summary>
		[Space, Title("LEVEL TILE SETTINGS", TitleAlignment = TitleAlignments.Centered, HorizontalLine = false), Space]
		[Tooltip("Width of the level tile section.")]
		[Range(6f, 20f)]
		[SerializeField]
		private float levelTileSectionWidth = 6f;
		
		/// <summary>
		/// Distance between level tile grid rows.
		/// </summary>
		[Tooltip("Distance between level tile grid rows.")]
		[Range(1f, 10f), OnValueChanged("GenerateLevelTileGrid")]
		[SerializeField]
		private float distanceBetweenLevelTileGridRows = 1f;
        
		/// <summary>
		/// Offset along the x axis to place points on the grid that represent side lanes.
		/// </summary>
        [Tooltip("Offset along the x axis to place points on the grid that represent side lanes.")]
        [Range(1f, 10f), OnValueChanged("GenerateLevelTileGrid")]
		[SerializeField]
		private float sideLaneXOffsetFromCenter = 2f;

        /// <summary>
        ///     Object that stores adata about sizes of various level chunks.
        /// </summary>
        [Space, Title("CHUNK SETTINGS", TitleAlignment = TitleAlignments.Centered, HorizontalLine = false), Space]
        [SerializeField]
		private LevelChunkSizeData levelChunkSizeData = new LevelChunkSizeData();

		/// <summary>
        ///     Should the level tile grid be shown in the editor?
        /// </summary>
        [Space, EditorHeader, Space]
        [Tooltip("Should the level tile grid be shown in the editor?")] 
        [SerializeField]
		private bool showLevelTileGrid;

        /// <summary>
        /// Size of level chunks to show in the level tile grid in the editor.
        /// </summary>
        [Tooltip("Size of level chunks to show in the level tile grid in the editor.")]
        [SerializeField]
        private LevelChunkSize chunkSizeToShow = LevelChunkSize.Tiny;

        /// <summary>
        /// Should the level tile chunk be shown in the editor?
        /// </summary>
        [Tooltip("Should the level tile chunk be shown in the editor?")]
        [SerializeField]
		private bool showLevelChunkGrid;

		/// <summary>
		/// When True, enables snapping of objects on the level chunk grid.
		/// </summary>
		[Tooltip("When True, enables snapping of objects on the level chunk grid.")]
		[SerializeField]
		private bool enableLevelChunkGridSnapping = false;

		#endregion

		#region Constants

        /// <summary>
        /// Size of the level tile/chunk grids along the grid's x axis.
        /// </summary>
        public const int GridSizeX = 3;

		#endregion

		#region Get

        /// <summary>
        /// Gets chunk size in level tile grid units for the passed chunk size type.
        /// </summary>
        /// <param name="chunkSizeType">Chunk size type.</param>
        /// <returns>
        /// An integer that shows how many level tiles cells a chunk spans along the level tile grid's y axis.
        /// </returns>
        public int GetChunkSizeByChunkSizeType(LevelChunkSize chunkSizeType)
		{
			return levelChunkSizeData.GetChunkDataByChunkSize(chunkSizeType).ChunkSize;
		}

		#endregion

#if UNITY_EDITOR

		#region Editor

		[Button("Generate Level Tile Grid")]
		private void GenerateLevelTileGrid()
		{
			PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

			if (prefabStage == null)
			{
				Debug.LogWarning("Trying to generate level tile grid when level tile prefab is not open! Please open the LevelTile prefab and try again.");
				return;
			}

			LevelTileGrid levelTileGrid = null;
			
			foreach (GameObject rootObject in prefabStage.scene.GetRootGameObjects())
			{
				levelTileGrid = rootObject.GetComponentInChildren<LevelTileGrid>();

				if (levelTileGrid != null)
				{
					break;
				}
			}

			if (levelTileGrid == null)
			{
				Debug.LogError("LevelTile prefab doesn't have a LevelTileGrid component!");
				return;
			}
			
			levelTileGrid.GenerateGrid();
		}

		#endregion

#endif
	}
}