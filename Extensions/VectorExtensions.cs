﻿using UnityEngine;

namespace Unidork.Extensions
{
    public static class VectorExtensions
    {
        #region Set

        /// <summary>
        /// Sets the x value of a Vector3 and returns the new vector.
        /// </summary>
        /// <param name="vector">Vector3 to change.</param>
        /// <param name="newX">New x value.</param>
        /// <returns>
        /// A Vector3 with a new x value.
        /// </returns>
        public static Vector3 SetX(this Vector3 vector, float newX)
        {
            vector.x = newX;
            return vector;
        }

        /// <summary>
        /// Sets the y value of a Vector3 and returns the new vector.
        /// </summary>
        /// <param name="vector">Vector3 to change.</param>
        /// <param name="newY">New y value.</param>
        /// <returns>
        /// A Vector3 with a new y value.
        /// </returns>
        public static Vector3 SetY(this Vector3 vector, float newY)
        {
            vector.y = newY;
            return vector;
        }

        /// <summary>
        /// Sets the z value of a Vector3 and returns the new vector.
        /// </summary>
        /// <param name="vector">Vector3 to change.</param>
        /// <param name="newZ">New z value.</param>
        /// <returns>
        /// A Vector3 with a new z value.
        /// </returns>
        public static Vector3 SetZ(this Vector3 vector, float newZ)
        {
            vector.z = newZ;
            return vector;
        }

        /// <summary>
        /// Sets the x value of a Vector2 and returns the new vector.
        /// </summary>
        /// <param name="vector">Vector2 to change.</param>
        /// <param name="newX">New x value.</param>
        /// <returns>
        /// A Vector2 with a new x value.
        /// </returns>
        public static Vector2 SetX(this Vector2 vector, float newX)
        {
            vector.x = newX;
            return vector;
        }

        /// <summary>
        /// Sets the y value of a Vector2 and returns the new vector.
        /// </summary>
        /// <param name="vector">Vector2 to change.</param>
        /// <param name="newY">New y value.</param>
        /// <returns>
        /// A Vector2 with a new y value.
        /// </returns>
        public static Vector2 SetY(this Vector2 vector, float newY)
        {
            vector.y = newY;
            return vector;
        }

        #endregion
    }
}