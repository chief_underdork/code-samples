﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Unidork.Extensions
{
	public static class GameObjectExtensions
	{
		#region Fields

		/// <summary>
		/// List to serve as a component cache to avoid memory allocations.
		/// </summary>
		private static List<Component> componentCache = new List<Component>();

		#endregion

		#region Get

		/// <summary>
		/// Gets a component on a game object without allocating memory.
		/// </summary>
		/// <param name="type">Type of component to get.</param>
		/// <param name="gameObject">Game object to get the component from.</param>
		/// <returns>
		/// An instance of <paramref name="type"/> or null if <paramref name="gameObject"/> doesn't have a component of specified type attached.
		/// </returns>
		public static Component GetComponentNonAlloc(this GameObject gameObject, Type type)
		{
			gameObject.GetComponents(type, componentCache);
			var component = componentCache.Count > 0 ? componentCache[0] : null;
			componentCache.Clear();
			return component;
		}

		/// <summary>
		/// Gets a component on a game object without allocating memory.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="gameObject">Game object to get the component from.</param>
		/// <returns>
		/// An instance of <typeparamref name="T"/> or null if <paramref name="gameObject"/> doesn't have a component of specified type attached.
		/// </returns>
		public static T GetComponentNonAlloc<T>(this GameObject gameObject) where T : Component
		{
			gameObject.GetComponents(typeof(T), componentCache);
			var component = componentCache.Count > 0 ? componentCache[0] : null;
			componentCache.Clear();
			return (T)component;
		}

		/// <summary>
		/// Gets a component of specified type or adds it to a game object and returns.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="gameObject">Game object that needs to get checked for a component of type <typeparamref name="T"/>.</param>
		/// <returns>
		/// Existing component of type <typeparamref name="T"/> or a new one if it doesn't exist.
		/// </returns>
		public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
		{
			T componentToReturn = gameObject.GetComponentNonAlloc<T>();

			if (componentToReturn == null)
			{
				componentToReturn = gameObject.AddComponent<T>();
			}

			return componentToReturn;
		}

		/// <summary>
		/// Gets the first located component of type <typeparamref name="T"/> in a game object's hierarchy, ignoring the game object's transform.
		/// Children of the object are acquired through a breadth-first search.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="gameObject">Game object whose hierarchy to search.</param>
		/// <returns>
		/// Component of type <typeparamref name="T"/> or null if none of game object's children have the component.
		/// </returns>
		public static T GetComponentInChildrenOnly<T>(this GameObject gameObject) where T : Component
		{
			List<Transform> children = gameObject.transform.GetAllChildren();

			T componentToReturn = null;

			foreach (Transform child in children)
			{
				componentToReturn = child.GetComponentNonAlloc<T>();

				if (componentToReturn != null)
				{
					return componentToReturn;
				}
			}

			return componentToReturn;
		}

		/// <summary>
		/// Gets all components of type <typeparamref name="T"/> in a game object's hierarchy, ignoring the game object's transform.
		/// Children of the object are acquired through a breadth-first search.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="gameObject">Game object whose hierarchy to search.</param>
		/// <returns>
		/// List of components of type <typeparamref name="T"/> that game object's children have.
		/// </returns>
		public static List<T> GetComponentsInChildrenOnly<T>(this GameObject gameObject) where T : Component
		{
			List<Transform> children = gameObject.transform.GetAllChildren();

			var componentsToReturn = new List<T>();

			foreach (Transform child in children)
			{
				T componentToAdd = child.GetComponentNonAlloc<T>();

				if (componentToAdd != null)
				{
					componentsToReturn.Add(componentToAdd);
				}
			}

			return componentsToReturn;
		}

		/// <summary>
		/// Gets the first located component of type <typeparamref name="T"/> in game object's parents, ignoring the game object's transform.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="gameObject">Game object whose parents to search.</param>
		/// <returns>
		/// Component of type <typeparamref name="T"/> or null if none of game object's parents have the component.
		/// </returns>
		public static T GetComponentInParentsOnly<T>(this GameObject gameObject) where T : Component
		{
			Transform parent = gameObject.transform.parent;

			while (parent != null)
			{
				T componentToReturn = parent.GetComponentNonAlloc<T>();

				if (componentToReturn != null)
				{
					return componentToReturn;
				}
				
				parent = gameObject.transform.parent;
			}

			return null;
		}

		/// <summary>
		/// Gets all components of type <typeparamref name="T"/> in game object's parents, ignoring the game object's transform.
		/// Children of the object are acquired through a breadth-first search.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Game object whose parents to search.</param>
		/// <returns>
		/// List of components of type <typeparamref name="T"/> that game object's parents have.
		/// </returns>
		public static List<T> GetComponentsInParentsOnly<T>(this GameObject gameObject) where T : Component
		{
			var componentsToReturn = new List<T>();

			Transform parent = gameObject.transform.parent;

			while (parent != null)
			{
				T componentToAdd = parent.GetComponentNonAlloc<T>();

				if (componentToAdd != null)
				{
					componentsToReturn.Add(componentToAdd);
				}
				
				parent = gameObject.transform.parent;
			}

			return componentsToReturn;
		}

		#endregion
	}
}