﻿using System.Text.RegularExpressions;

namespace Unidork
{
	public static class StringExtensions
	{

		/// <summary>
		/// Splits a string by capital letters.
		/// </summary>
		/// <param name="string">String to split.</param>
		/// <returns>
		/// <paramref name="string"/> with spaces inserted before capital letters.
		/// </returns>
		public static string SplitByCapitalLetters(this string @string)
		{
			var regex = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z]) | (?<=[^A-Z])(?=[A-Z]) | (?<=[A-Za-z])(?=[^A-Za-z])",
								  RegexOptions.IgnorePatternWhitespace);

			return regex.Replace(@string, " ");
		}
	}
}