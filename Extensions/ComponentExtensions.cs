﻿using System.Collections.Generic;
using UnityEngine;

namespace Unidork.Extensions
{
	public static class ComponentExtensions
	{
		#region Get

		/// <summary>
		/// Gets a component on a game object another component is attached to without allocating memory.
		/// </summary>
		/// <param name="type">Type of component to get.</param>
		/// <param name="component">Component from whose game object to get another component.</param>
		/// <returns>
		/// An instance of <paramref name="type"/> or null if component's game object doesn't have a component of specified type attached.
		/// </returns>
		public static Component GetComponentNonAlloc(this Component component, System.Type type) => component.gameObject.GetComponentNonAlloc(type);

		/// <summary>
		/// Gets a component on a game object another component is attached to without allocating memory.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component from whose game object to get another component.</param>
		/// <returns>
		/// An instance of <typeparamref name="T"/> or null if component's game object doesn't have a component of specified type attached.
		/// </returns>
		public static T GetComponentNonAlloc<T>(this Component component) where T : Component => component.gameObject.GetComponentNonAlloc<T>();

		/// <summary>
		/// Gets a component of specified type or adds it to a game object and returns.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component whose game object needs to get checked for a component of type <typeparamref name="T"/>.</param>
		/// <returns>
		/// Existing component of type <typeparamref name="T"/> or a new one if it doesn't exist.
		/// </returns>
		public static T GetOrAddComponent<T>(this Component component) where T : Component
		{
			T componentToReturn = component.GetComponentNonAlloc<T>();

			if (componentToReturn == null)
			{
				componentToReturn = component.gameObject.AddComponent<T>();
			}

			return componentToReturn;
		}

		/// <summary>
		/// Gets the first located component of type <typeparamref name="T"/> in a passed component's game object hierarchy, ignoring the component's transform.
		/// Children of the component's game object are acquired through a breadth-first search.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component whose hierarchy to search.</param>
		/// <returns>
		/// Component of type <typeparamref name="T"/> or null if none of component's game object children have the component.
		/// </returns>
		public static T GetComponentInChildrenOnly<T>(this Component component) where T : Component
		{
			List<Transform> children = component.transform.GetAllChildren();

			T componentToReturn = null;

			foreach (Transform child in children)
			{
				componentToReturn = child.GetComponentNonAlloc<T>();

				if (componentToReturn != null)
				{
					return componentToReturn;
				}
			}

			return componentToReturn;
		}

		/// <summary>
		/// Gets all components of type <typeparamref name="T"/> in a component's game object hierarchy, ignoring the component's game object transform.
		/// Children of the object are acquired through a breadth-first search.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component whose hierarchy to search.</param>
		/// <returns>
		/// List of components of type <typeparamref name="T"/> that game object's children have.
		/// </returns>
		public static List<T> GetComponentsInChildrenOnly<T>(this Component component) where T : Component
		{
			List<Transform> children = component.transform.GetAllChildren();

			var componentsToReturn = new List<T>();

			foreach (Transform child in children)
			{
				T componentToAdd = child.GetComponentNonAlloc<T>();

				if (componentToAdd != null)
				{
					componentsToReturn.Add(componentToAdd);
				}
			}

			return componentsToReturn;
		}

		/// <summary>
		/// Gets the first located component of type <typeparamref name="T"/> in component's game object parents, ignoring the component's game object transform.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component whose game object's parents to search.</param>
		/// <returns>
		/// Component of type <typeparamref name="T"/> or null if none of component's game object parents have the component.
		/// </returns>
		public static T GetComponentInParentsOnly<T>(this Component component) where T : Component
		{
			Transform parent = component.transform.parent;

			while (parent != null)
			{
				T componentToReturn = parent.GetComponentNonAlloc<T>();

				if (componentToReturn != null)
				{
					return componentToReturn;
				}
				
				parent = component.transform.parent;
			}

			return null;
		}

		/// <summary>
		/// Gets all components of type <typeparamref name="T"/> in component's game object parents, ignoring the component's game object transform.
		/// </summary>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <param name="component">Component whose game object's parents to search.</param>
		/// <returns>
		/// List of components of type <typeparamref name="T"/> that game object's parents have.
		/// </returns>
		public static List<T> GetComponentsInParentsOnly<T>(this Component component) where T : Component
		{
			var componentsToReturn = new List<T>();

			Transform parent = component.transform.parent;

			while (parent != null)
			{
				T componentToAdd = parent.GetComponentNonAlloc<T>();

				if (componentToAdd != null)
				{
					componentsToReturn.Add(componentToAdd);
				}

				parent = component.transform.parent;
			}

			return componentsToReturn;
		}

		#endregion
	}
}