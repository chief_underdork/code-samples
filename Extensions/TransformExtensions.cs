﻿using System.Collections.Generic;
using UnityEngine;

namespace Unidork.Extensions
{
    public static class TransformExtensions
    {
        #region General

        /// <summary>
        /// Sets transform's position, rotation and scale.
        /// </summary>
        /// <param name="transform">Target transform.</param>
        /// <param name="position">Position to set.</param>
        /// <param name="rotation">Rotation to set.</param>
        /// <param name="scale">Scale to set.</param>
        /// <param name="setWorldPosition">Should world position be set or local?</param>
        /// <param name="setWorldRotation">Should world rotation be set or local?</param>
        public static void SetPositionRotationAndScale(this Transform transform, Vector3 position, Quaternion rotation, Vector3 scale,
                                                       bool setWorldPosition = true, bool setWorldRotation = true)
        {
            if (setWorldPosition)
            {
                transform.position = position;
            }
            else
            {
                transform.localPosition = position;
            }

            if (setWorldRotation)
            {
                transform.rotation = rotation;
            }
            else
            {
                transform.localRotation = rotation;
            }

            transform.localScale = scale;
        }

        /// <summary>
        /// Sets transform's position, rotation and scale.
        /// </summary>
        /// <param name="transform">Target transform.</param>
        /// <param name="position">Position to set.</param>
        /// <param name="eulerAngles">Euler angles that represent rotation to set.</param>
        /// <param name="scale">Scale to set.</param>
        /// <param name="setWorldPosition">Should world position be set or local?</param>
        /// <param name="setWorldRotation">Should world rotation be set or local?</param>
        public static void SetPositionRotationAndScale(this Transform transform, Vector3 position, Vector3 eulerAngles, Vector3 scale,
                                                       bool setWorldPosition = true, bool setWorldRotation = true)
        {
            SetPositionRotationAndScale(transform, position, Quaternion.Euler(eulerAngles), scale, setWorldPosition, setWorldRotation);
        }

        #endregion

        #region Search

        /// <summary>
        /// Performs a breadht-first deep search of a transform's hierarchy for an object with specified name.
        /// </summary>
        /// <param name="transform">Transform whose hierarchy will be searched.</param>
        /// <param name="childName">Name of object to search for.</param>
        /// <returns>
        /// A transform whose name matches <paramref name="childName"/> or null if no such object exists.
        /// </returns>
        public static Transform FindDeepChild(this Transform transform, string childName)
        {
            var transformQueue = new Queue<Transform>();
            transformQueue.Enqueue(transform);

            while (transformQueue.Count > 0)
            {
                var childTransform = transformQueue.Dequeue();

                if (childTransform.name == childName)
                {
                    return childTransform;
                }                    

                foreach (Transform grandChildTransform in childTransform)
                {
                    transformQueue.Enqueue(grandChildTransform);
                }                    
            }
            return null;
        }

        /// <summary>
        /// Gets all children in transform's hierarchy.
        /// </summary>
        /// <param name="transform">Transform to get the children from.</param>
        /// <returns>
        /// A hash set of transforms that are children of the target transform.
        /// </returns>
        public static List<Transform> GetAllChildren(this Transform transform)
        {
            var children = new List<Transform>();
            var transformQueue = new Queue<Transform>();
            transformQueue.Enqueue(transform);

            while (transformQueue.Count > 0)
            {
                var childTransform = transformQueue.Dequeue();
                children.Add(childTransform);

                foreach (Transform grandChildTransform in childTransform)
                {
                    transformQueue.Enqueue(grandChildTransform);
                }
            }

            children.Remove(transform);

            return children;
        }

        #endregion

        #region Transform point

        /// <summary>
        /// Transforms a point, ignoring object's scale.
        /// </summary>
        /// <param name="transform">Transform component to use for the operation.</param>
        /// <param name="position">Point to transform.</param>
        /// <returns>
        /// A Vector3 representing a transformed position, unaffected by object's scale.
        /// </returns>
        public static Vector3 TransformPointUnscaled(this Transform transform, Vector3 position)
        {
            var localToWorldMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            return localToWorldMatrix.MultiplyPoint3x4(position);
        }

        /// <summary>
        /// Inverse-transforms a point, ignoring object's scale.
        /// </summary>
        /// <param name="transform">Transform component to use for the operation.</param>
        /// <param name="position">Point to transform.</param>
        /// <returns>
        /// A Vector3 representing an inversed transformed position, unaffected by object's scale.
        /// </returns>
        public static Vector3 InverseTransformPointUnscaled(this Transform transform, Vector3 position)
        {
            var worldToLocalMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
            return worldToLocalMatrix.MultiplyPoint3x4(position);
        }


        #endregion
    }
}