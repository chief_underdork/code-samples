﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Unidork.Extensions
{
    public static class CollectionExtensions
    {
        #region General

        /// <summary>
        /// Checks whether a collection is null or empty.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to check.</param>
        /// <returns>
        /// True when collection is either null or empty, False otherwise.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
        {
            return collection == null || collection.Count == 0;
        }

        /// <summary>
        /// Checks whether an enumerable sequence is null or empty.
        /// </summary>
        /// <param name="collection">Sequence to check.</param>
        /// <returns>
        /// True if sequence is null or empty, False otherwise.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection) => collection == null || !collection.Any();

        #endregion

        #region Random

        /// <summary>
        /// Gets a random item in an a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the random element from.</param>
        /// <returns>
        /// A random element of the collection, or default value of <typeparamref name="T"/> if collection is null or empty.
        /// </returns>
        public static T GetRandomOrDefault<T>(this IList<T> collection)
        {
            if (collection == null)
            {
                return default;
            }

            int count = collection.Count;

            if (count == 0)
            {
                return default;
            }

            int randomIndex = Random.Range(0, count);

            return collection[randomIndex];
        }

        /// <summary>
        /// Gets a random item in an a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the random element from.</param>
        /// <param name="defaultValue">Default value to return if collection is null or empty.</param>
        /// <returns>
        /// A random element of the collection, or the provided default value if collection is null or empty
        /// </returns>
        public static T GetRandomOrDefault<T>(this IList<T> collection, T defaultValue)
        {
            if (collection == null)
            {
                return defaultValue;
            }

            int count = collection.Count;

            if (count == 0)
            {
                return defaultValue;
            }

            int randomIndex = Random.Range(0, count);

            return collection[randomIndex];
        }

        /// <summary>
        /// Gets a random item in an a set.
        /// </summary>
        /// <remarks>
        /// This method is required in addition to <see cref="GetRandomOrDefault{T}(IList{T})"/> since sets do not implement IList.
        /// </remarks>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="set">Set to get the random element from.</param>
        /// <returns>
        /// A random element of a set, or default value of <typeparamref name="T"/> if set is null or empty.
        /// </returns>
        public static T GetRandomOrDefault<T>(this ISet<T> set)
        {
            if (set == null)
            {
                return default;
            }

            int count = set.Count;

            if (count == 0)
            {
                return default;
            }

            int randomIndex = Random.Range(0, count);
            int currentIndex = 0;

            foreach (var element in set)
            {
                if (currentIndex == randomIndex)
                {
                    return element;
                }

                currentIndex++;
            }

            return default;
        }

        /// <summary>
        /// Gets a random item in an a set.
        /// </summary>
        /// <remarks>
        /// This method is required in addition to <see cref="GetRandomOrDefault{T}(IList{T}, T)"/> since sets do not implement IList.
        /// </remarks>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="set">Set to get the random element from.</param>
        /// <param name="defaultValue">Default value to return if set is null or empty.</param>
        /// <returns>
        /// A random element of the set, or the provided default value if collection is null or empty.
        /// </returns>
        public static T GetRandomOrDefault<T>(this ISet<T> set, T defaultValue)
        {
            if (set == null)
            {
                return defaultValue;
            }

            int count = set.Count;

            if (count == 0)
            {
                return defaultValue;
            }

            int randomIndex = Random.Range(0, count);
            int currentIndex = 0;

            foreach (var element in set)
            {
                if (currentIndex == randomIndex)
                {
                    return element;
                }

                currentIndex++;
            }

            return defaultValue;
        }

        #endregion

        #region Index

        /// <summary>
        /// Gets the first element of a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the first element of.</param>
        /// <returns>
        /// The first element of the collection or default value of <typeparamref name="T"/> if collection is null or empty.
        /// </returns>
        public static T First<T>(this IList<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return default;
            }

            return collection[0];
        }

        /// <summary>
        /// Gets the first element of an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the sequence.</typeparam>
        /// <param name="collection">Enumerable sequence to get the first element from.</param>
        /// <returns>
        /// First element of the sequence or default value of <typeparamref name="T"/> when the sequence is null or empty.
        /// </returns>
        public static T FirstElement<T>(this IEnumerable<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return default;
            }

            return collection.First();
        }

        /// <summary>
        /// Gets the first element of a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the first element of.</param>
        /// <param name="defaultValue">Default value to return if collection is null or empty.</param>
        /// <returns>
        /// The first element of the collection or the provided default value if collection is null or empty.
        /// </returns>
        public static T FirstOrDefault<T>(this IList<T> collection, T defaultValue)
        {
            if (collection.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return collection[0];
        }

        /// <summary>
        /// Gets the first element of an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the enumerable sequence.</typeparam>
        /// <param name="collection">Enumerable sequence to get the first element of.</param>
        /// <param name="defaultValue">Default value to return if enumerable sequence is null or empty.</param>
        /// <returns>
        /// The first element of the enumerable sequence or the provided default value if collection is null or empty.
        /// </returns>
        public static T FirstOrDefault<T>(this IEnumerable<T> collection, T defaultValue)
        {
            if (collection.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return collection.First();
        }

        /// <summary>
        /// Gets the last element of a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the last element of.</param>
        /// <returns>
        /// The last element of the collection or default value of <typeparamref name="T"/> if collection is null or empty.
        /// </returns>
        public static T Last<T>(this IList<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return default;
            }

            return collection[collection.Count - 1];
        }

        /// <summary>
        /// Gets the last element of an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the sequence.</typeparam>
        /// <param name="collection">Enumerable sequence to get the last element from.</param>
        /// <returns>
        /// Last element of the sequence or default value of <typeparamref name="T"/> when the sequence is null or empty.
        /// </returns>
        public static T LastElement<T>(this IEnumerable<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return default;
            }

            return collection.Last();
        }

        /// <summary>
        /// Gets the last element of a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to get the last element of.</param>
        /// <param name="defaultValue">Default value to return if collection is null or empty.</param>
        /// <returns>
        /// The last element of the collection or the provided default value if collection is null or empty.
        /// </returns>
        public static T LastOrDefault<T>(this IList<T> collection, T defaultValue)
        {
            if (collection.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return collection[collection.Count - 1];
        }

        /// <summary>
        /// Gets the last element of an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the enumerable sequence.</typeparam>
        /// <param name="collection">Enumerable sequence to get the last element of.</param>
        /// <param name="defaultValue">Default value to return if enumerable sequence is null or empty.</param>
        /// <returns>
        /// The last element of the enumerable sequence or the provided default value if collection is null or empty.
        /// </returns>
        public static T LastOrDefault<T>(this IEnumerable<T> collection, T defaultValue)
        {
            if (collection.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return collection.Last();
        }

        #endregion

        #region Add/Remove

        /// <summary>
        /// Adds an element to a collection only if it doesn't contain it already and the element is not null.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to add the element to.</param>
        /// <param name="elementToAdd">Element to add.</param>
        /// <returns>
        /// True when an element is successfully added to the collection, False otherwise.
        /// </returns>
        public static bool AddUnique<T>(this ICollection<T> collection, T elementToAdd)
        {
            if (elementToAdd == null || collection.Contains(elementToAdd))
            {
                return false;
            }

            collection.Add(elementToAdd);
            return true;
        }

        /// <summary>
        /// Adds an element to a collection only if the element is not null.
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to add the element to.</param>
        /// <param name="elementToAdd">Element to add.</param>
        /// <returns>
        /// True when an element is successfully added to the collection, False otherwise.
        /// </returns>
        public static bool AddIfNotNull<T>(this ICollection<T> collection, T elementToAdd)
        {
            if (elementToAdd == null)
            {
                return false;
            }

            collection.Add(elementToAdd);
            return true;
        }

        /// <summary>
        /// Adds an element to a collection at the specified index.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to add the element to.</param>
        /// <param name="elementToAdd">Element to add.</param>
        /// <param name="index">Index at which to add the element.</param>
        /// <returns>
        /// True when the element is successfully added, False otherwise.
        /// </returns>
        public static bool AddAt<T>(this IList<T> collection, T elementToAdd, int index)
        {
            if (collection == null)
            {
                Debug.LogError("Trying to add an element to a null collection. Make sure you initialize the collection first!");
                return false;
            }

            collection.Insert(index, elementToAdd);
            return true;
        }

		/// <summary>
		/// Adds elements from one collection to another.
		/// </summary>
		/// <typeparam name="T">Element type of the collection.</typeparam>
		/// <param name="collection">Collection to add missing elements to.</param>
		/// <param name="otherCollection">Other collection that contains potential elements to add.</param>
		/// <returns>
		/// Number of elements that were added.
		/// </returns>
		public static int AddRange<T>(this ICollection<T> collection, ICollection<T> otherCollection)
		{
			var numberOfAddedElements = 0;

			if (collection == null)
			{
				Debug.LogError("AddRange was called to add elements to a null collection!");
				return numberOfAddedElements;
			}

			if (otherCollection == null)
			{
				Debug.LogError("AddRange was called to add elements from a null collection!");
				return numberOfAddedElements;
			}

			if (otherCollection.Count == 0)
			{
				Debug.LogWarning("Trying to add elements from an empty collection!");
				return numberOfAddedElements;
			}

			foreach (var otherCollectionElement in otherCollection)
			{
				collection.Add(otherCollectionElement);				
				numberOfAddedElements++;				
			}

			return numberOfAddedElements;
		}

        /// <summary>
        /// Adds elements to a collection from another collection that the first collection doesn't have.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to add missing elements to.</param>
        /// <param name="otherCollection">Other collection that contains potential elements to add.</param>
		/// <returns>
		/// Number of elements that were added.
		/// </returns>
        public static int AddRangeUnique<T>(this ICollection<T> collection, ICollection<T> otherCollection)
        {
			var numberOfAddedElements = 0;

			if (collection == null)
			{
				Debug.LogError("AddRangeUnique was called to add elements to a null collection!");
				return numberOfAddedElements;
			}

			if (otherCollection == null)
			{
				Debug.LogError("AddRangeUnique was called to add elements from a null collection!");
				return numberOfAddedElements;
			}

			if (otherCollection.Count == 0)
			{
				Debug.LogWarning("Trying to add elements from an empty collection!");
				return numberOfAddedElements;
			}

			foreach (var otherCollectionElement in otherCollection)
			{
				if (collection.AddUnique(otherCollectionElement))
				{
					numberOfAddedElements++;
				}
			}

			return numberOfAddedElements;
        }

        /// <summary>
        /// Removes the first element from a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to remove the first element of.</param>
        /// <returns>
        /// True when an element is successfully removed, False otherwise.
        /// </returns>
        public static bool RemoveFirst<T>(this IList<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return false;
            }

            collection.RemoveAt(0);

            return true;
        }

        /// <summary>
        /// Removes the last element from a collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to remove the last element of.</param>
        /// <returns>
        /// True when an element is successfully removed, False otherwise.
        /// </returns>
        public static bool RemoveLast<T>(this IList<T> collection)
        {
            if (collection.IsNullOrEmpty())
            {
                return false;
            }

            collection.RemoveAt(collection.Count - 1);

            return true;
        }


        /// <summary>
        /// Removes all elements in a collection that match an element in another collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to remove matching elements from.</param>
        /// <param name="otherCollection">Other collection that contains potential matches.</param>
        public static void RemoveMatchingElements<T>(this ICollection<T> collection, ICollection<T> otherCollection)
        {
            HashSet<T> elementsToRemove = new HashSet<T>();
            
            foreach (var otherCollectionElement in otherCollection)
            {
                if (collection.Contains(otherCollectionElement))
                {
                    elementsToRemove.Add(otherCollectionElement);
                }
            }

            foreach (var elementToRemove in elementsToRemove)
            {
                collection.Remove(elementToRemove);
            }
        }

        #endregion

        #region Contain

		/// <summary>
		/// Checks whether an array contains a specified element.
		/// </summary>
		/// <typeparam name="T">Type of array and element.</typeparam>
		/// <param name="array">Array to check.</param>
		/// <param name="elementToCheckFor">Element to check for.</param>
		/// <returns>
		/// True if <paramref name="array"/> contains <paramref name="elementToCheckFor"/>, False if it doesn't or if the array is null or empty.
		/// </returns>
		public static bool Contains<T>(this T[] array, T elementToCheckFor)
		{
			if (array.IsNullOrEmpty())
			{
				return false;
			}

			for (int i = 0; i < array.Length; i++)
			{
				if (array[i].Equals(elementToCheckFor))
				{
					return true;
				}
			}

			return false;
		}

        /// <summary>
        /// Checks whether a collection contains any element of another collection.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Collection to check.</param>
        /// <param name="otherCollection">Other collection.</param>
        /// <returns>
        /// True when collections have at least one common element, False otherwise.
        /// </returns>
        public static bool ContainsAnyOfElements<T>(this ICollection<T> collection, ICollection<T> otherCollection)
        {
            foreach (var otherCollectionElement in otherCollection)
            {
                if (collection.Contains(otherCollectionElement))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region To Collection

        /// <summary>
        /// Creates a list from an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Sequence to use for creating the list.</param>        
        /// <returns>
        /// A list containing the passed collection.
        /// </returns>
        public static List<T> ToList<T>(this IEnumerable<T> collection) => new List<T>(collection);

        /// <summary>
        /// Creates a hash set from an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Sequence to use for creating the hash set.</param>
        /// <param name="comparer">Equality comparer to be used by the hash set.</param>
        /// <returns>
        /// A hash set containing the passed collection and using the passed comparer.
        /// </returns>
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> collection, IEqualityComparer<T> comparer = null) => new HashSet<T>(collection, comparer);

        /// <summary>
        /// Creates a queue from an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Sequence to use for creating the queue.</param>        
        /// <returns>
        /// A queue containing the passed collection.
        /// </returns>
        public static Queue<T> ToQueue<T>(this IEnumerable<T> collection) => new Queue<T>(collection);

        /// <summary>
        /// Creates a stack from an enumerable sequence.
        /// </summary>
        /// <typeparam name="T">Element type of the collection.</typeparam>
        /// <param name="collection">Sequence to use for creating the stack.</param>        
        /// <returns>
        /// A stack containing the passed collection.
        /// </returns>
        public static Stack<T> ToStack<T>(this IEnumerable<T> collection) => new Stack<T>(collection);

        #endregion
    }
}