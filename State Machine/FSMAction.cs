﻿using UnityEngine;

namespace MergeSea
{
	/// <summary>
	/// Base class for scriptable objects that represents finite state machine actions.
	/// </summary>
	public abstract class FSMAction : ScriptableObject
	{
		public abstract void Perform(FSMStateController stateController);
	}
}
