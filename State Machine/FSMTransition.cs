﻿using Unidork.Attributes;
using UnityEngine;

namespace MergeSea
{
	/// <summary>
	/// Represents a transition between states in a finite state machine.
	/// </summary>
	/// <typeparam name="T">Type of data used in the finite state machine.</typeparam>
	[System.Serializable]
	public class FSMTransition
	{
		#region Properties

		/// <summary>
		/// Decision to use to decide the outcome of the transition.
		/// </summary>
		/// <value>
		/// Gets the value of the field decision.
		/// </value>
		public FSMDecision Decision => decision;

		/// <summary>
		///  State to go to when decision returns a positive evaluation.
		/// </summary>
		/// <value>
		/// Gets the value of the field stateWhenTrue.
		/// </value>
		public FSMState StateWhenTrue => stateWhenTrue;

		/// <summary>
		///  State to go to when decision returns a negative evaluation.
		/// </summary>
		/// <value>
		/// Gets the value of the field stateWhenFalse.
		/// </value>
		public FSMState StateWhenFalse => stateWhenFalse;

		#endregion

		#region Fields

		/// <summary>
		/// Decision to use to decide the outcome of the transition.
		/// </summary>
		[Space, SettingsHeader, Space]
		[Tooltip("Decision to use to decide the outcome of the transition.")]
		[SerializeField]
		protected FSMDecision decision = null;

		/// <summary>
		/// State to go to when decision returns a positive evaluation.
		/// </summary>
		[Tooltip("State to go to when decision returns a positive evaluation.")]
		[SerializeField]
		protected FSMState stateWhenTrue = null;

		/// <summary>
		/// State to go to when decision returns a negative evaluation.
		/// </summary>
		[Tooltip("State to go to when decision returns a positive evaluation.")]
		[SerializeField]
		protected FSMState stateWhenFalse = null;

		#endregion
	}
}