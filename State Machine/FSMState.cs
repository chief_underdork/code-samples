﻿using Unidork.Attributes;
using Unidork.Extensions;
using UnityEngine;

namespace MergeSea
{
	/// <summary>
	/// Represents a state in a finite state machine.
	/// </summary>
	[CreateAssetMenu(fileName = "New State", menuName = "State Machine/New State", order = 0)]
	public class FSMState : ScriptableObject
	{
#if UNITY_EDITOR

		#region Properties

		public Color SceneGizmoColor => sceneGizmoColor;

		#endregion

		[Space, DebugHeader, Space]
		[SerializeField]
		private Color sceneGizmoColor = Color.gray;
#endif

		#region Fields

		/// <summary>
		/// Actions that can be performed while this state is the active one.
		/// </summary>
		[Space, AssetsHeader, Space]
		[Tooltip("Actions that can be performed while this state is the active one.")]
		[SerializeField]
		protected FSMAction[] actions = null;

		/// <summary>
		/// Transitions to other states to evaluate when this state is the active one.
		/// </summary>
		[Tooltip("Transitions to other states to evaluate when this state is the active one.")]
		[SerializeField]
		protected FSMTransition[] transitions = null;

		#endregion

		#region State

		/// <summary>
		/// Callback for entering the state.
		/// </summary>
		public virtual void OnEnterState() { }

		/// <summary>
		/// Callback for exitting the state.
		/// </summary>
		public virtual void OnExitState() { }

		/// <summary>
		/// Updates current state by first performing its actions and then checking whether a transition to another state should happen.
		/// </summary>
		/// <param name="stateController">State controller driving the behavior of owning object.</param>
		public void UpdateState(FSMStateController stateController)
		{
			PerformActions(stateController);
			CheckTransitions(stateController);
		}		

		/// <summary>
		/// Performs actions assigned to this state.
		/// </summary>
		/// <param name="stateController">State controller driving the behavior of owning object.</param>
		private void PerformActions(FSMStateController stateController)
		{
			if (actions.IsNullOrEmpty())
			{
				return;
			}

			foreach (FSMAction action in actions)
			{
				action.Perform(stateController);
			}
		}

		private void CheckTransitions(FSMStateController stateController)
		{
			foreach (FSMTransition transition in transitions)
			{
				bool decisionPositive = transition.Decision.Decide(stateController);

				if (decisionPositive)
				{
					stateController.TransitionToState(transition.StateWhenTrue);
					break;
				}
				else
				{
					FSMState stateWhenFalse = transition.StateWhenFalse;

					if (stateWhenFalse != null)
					{
						stateController.TransitionToState(stateWhenFalse);
						break;
					}
				}
			}
		}

		#endregion
	}
}