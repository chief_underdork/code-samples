﻿namespace MergeSea
{
	/// <summary>
	/// Base class for objects that hold data for a <see cref="FSMStateController"/>. 
	/// Required to allow the state controller to return data using generics.
	/// </summary>
	public abstract class FSMStateControllerData { }
}