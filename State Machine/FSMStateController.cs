﻿using Unidork.Attributes;
using UnityEngine;

namespace MergeSea
{
	/// <summary>
	/// Controllers that handles updating states of a finite state machine.
	/// </summary>
	public abstract class FSMStateController : MonoBehaviour
	{
		#region Properties

		/// <summary>
		/// Controller's current state.
		/// </summary>
		public FSMState CurrentState
		{
			get => currentState;

			set
			{
				// Safe to use the null conditional here since states are scriptable objects that are never destroyed.
				currentState?.OnExitState();
				currentState = value;
				currentState.OnEnterState();
			}
		}

		#endregion

		#region Fields		

		protected FSMStateControllerData stateControllerData;

		[Space, DebugHeader, Space]
		[SerializeField]
		private FSMState currentState;

		#endregion

		#region Data
		
		/// <summary>
		/// Gets state controller data. Used by derived classes to get specific types of controller data.
		/// </summary>
		/// <typeparam name="T">Type of data to get.</typeparam>
		/// <returns>
		/// <see cref="stateControllerData"/> cast to the specified type.
		/// </returns>
		public T GetStateControllerData<T>() where T : FSMStateControllerData
		{
			return (T)stateControllerData;
		}

		public abstract void UpdateStateControllerData();

		#endregion

		#region Enable/disable

		public void Enable() => enabled = true;

		public void Disable() => enabled = false;

		#endregion

		#region Update

		private void Update()
		{
			UpdateStateControllerData();

			// Safe to use the null conditional here since states are scriptable objects that are never destroyed.
			CurrentState?.UpdateState(this);
		}

		#endregion

		#region Transitions

		/// <summary>
		/// Transitions to a new state passed to the method.
		/// </summary>
		/// <param name="newState">New state.</param>
		public void TransitionToState(FSMState newState)
		{
			CurrentState = newState;
		}

		#endregion

		#region Editor

#if UNITY_EDITOR

		private void OnDrawGizmos()
		{
			if (CurrentState != null)
			{
				Color oldColor = Gizmos.color;

				Gizmos.color = CurrentState.SceneGizmoColor;
				Gizmos.DrawSphere(transform.position + Vector3.up, 0.5f);

				Gizmos.color = oldColor;
			}
		}
#endif

		#endregion
	}
}