﻿using System;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Unidork.ObjectPooling
{
    /// <summary>
    /// Manages a pool of objects that can be used by an <see cref="ObjectSpawner/>.
    /// </summary>
    public class ObjectPooler
    {
        #region Properties

        /// <summary>
        /// Pooler's name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Has this pooler finished creating the pool?
        /// </summary>
        public bool FinishedCreatingPool => numberOfCreatedLists == numberOfListsToCreate;

        #endregion
        
        #region Fields

        /// <summary>
        /// All object poolers that are currently active.
        /// </summary>
        private static List<ObjectPooler> objectPoolers;

        /// <summary>
        /// Settings for this object pooler.
        /// </summary>
        private readonly ObjectPoolerSettings settings;
        
        /// <summary>
        /// Transform that serves as a holder for pooled objects.
        /// </summary>
        private readonly Transform pooledObjectHolder;
        
        /// <summary>
        /// Dictionary where an asset guid is the key and a list of pooled objects corresponding with that asset are the value.
        /// </summary>
        private readonly Dictionary<string, List<IPooledObject>> pooledObjectLists;

        /// <summary>
        /// Number of object lists that need to be created by this pooler.
        /// </summary>
        private int numberOfListsToCreate = -1;
        
        /// <summary>
        /// Number of object lists already create by this pooler.
        /// </summary>
        private int numberOfCreatedLists;

        #endregion
        
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="settings">Object pooler settings.</param>
        public ObjectPooler(ObjectPoolerSettings settings)
        {
            Name = settings.Name;

            this.settings = settings;
            
            pooledObjectHolder = settings.PooledObjectHolder;
            
            pooledObjectLists = new Dictionary<string, List<IPooledObject>>();
            CreatePool(settings.GetPoolItemSettings());
        }

        #endregion

        #region Pool

        /// <summary>
        /// Creates the object pool from the passed settings. A separate list if created for each item in the passed array
        /// and stored as a value in a dictionary.
        /// </summary>
        /// <param name="settingsArray">Object pool item settings.</param>
        /// <exception cref="ArgumentException">Thrown when passed settings array is null or empty.</exception>
        private void CreatePool(ObjectPoolItemSettings[] settingsArray)
        {
            if (settingsArray.IsNullOrEmpty())
            {
                throw new ArgumentException($"{Name} has received an invalid array of pooled item settings!");
            }
            
            numberOfCreatedLists = 0;
            numberOfListsToCreate = settingsArray.Length;

            foreach (ObjectPoolItemSettings settings in settingsArray)
            {
                CreatePooledObjectListAfterAssetIsLoaded(settings);
            }
        }

        /// <summary>
        /// Loads the asset from the passed settings and calls <see cref="CreatePooledObjectList"/> in case of a successfull load.
        /// </summary>
        private void CreatePooledObjectListAfterAssetIsLoaded(ObjectPoolItemSettings settings)
        {
            AsyncOperationHandle itemHandle = Addressables.LoadAssetAsync<GameObject>(settings.Asset);

            itemHandle.Completed += itemLoaded =>
            {
                if (itemLoaded.Status == AsyncOperationStatus.Succeeded)
                {
                    CreatePooledObjectList(itemHandle, settings);
                }
                else
                {
                    Debug.LogError($"{Name} failed to load asset with guid {settings.Asset.RuntimeKey}");
                }
            };
        }
        
        /// <summary>
        /// Instantiates a pool of objects from the passed item handle and settings and adds it to the pooler dictionary.
        /// </summary>
        /// <param name="itemHandle">Item handle.</param>
        /// <param name="settings">Object pool item settings.</param>
        /// <exception cref="ArgumentException">Thrown when the pool already contains items matching the passed handle.</exception>
        private void CreatePooledObjectList(AsyncOperationHandle itemHandle, ObjectPoolItemSettings settings)
        {
            if (pooledObjectLists.ContainsKey(settings.Asset.RuntimeKey.ToString()))
            {
                numberOfCreatedLists++;
                throw new ArgumentException($"{Name} already contains a pool for asset with guid {settings.Asset.RuntimeKey}");
            }
            
            var pooledObjectList = new List<IPooledObject>();
            
            for (var i = 1; i <= settings.NumberToPool; i++)
            {    
                var pooledObject = Object.Instantiate((GameObject)itemHandle.Result, pooledObjectHolder).GetComponent<IPooledObject>();
                pooledObject.Deactivate();
                pooledObjectList.Add(pooledObject);
            }

            pooledObjectLists.Add(settings.Asset.RuntimeKey.ToString(), pooledObjectList);
            
            numberOfCreatedLists++;
            
            Addressables.Release(itemHandle);
        }

        /// <summary>
        /// Destroys all objects in the pool. ONLY USE when the pool is no longer needed.
        /// </summary>
        private void Destroy()
        {
            foreach (KeyValuePair<string, List<IPooledObject>> kvp in pooledObjectLists)
            {
                List<IPooledObject> pooledObjects = kvp.Value;

                while (pooledObjects.Count > 0)
                {
                    pooledObjects[0].Destroy();
                }
            }
            
            pooledObjectLists.Clear();
        }

        #endregion

        #region Items

        /// <summary>
        /// Gets an item from the pool that matches the passed asset reference.
        /// </summary>
        /// <param name="assetReference">Asset reference.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when the pool dictionary doesn't contain any items matching the passed asset.</exception>
        public IPooledObject Get(AssetReferenceGameObject assetReference)
        {
            if (!pooledObjectLists.TryGetValue(assetReference.RuntimeKey.ToString(), out List<IPooledObject> pooledObjects))
            {
                throw new ArgumentException($"{Name} doesn't contain items that match asset with guid {assetReference.RuntimeKey}");
            }

            foreach (IPooledObject pooledObject in pooledObjects)
            {
                if (!pooledObject.IsActive)
                {
                    return pooledObject;
                }
            }

            return null;
            
            //TODO: Handle pool expansion here.
        }

        #endregion
        
        #region Static

        /// <summary>
        /// Create a new object pooler with provided settings and adds it to the list of currently active poolers.
        /// </summary>
        /// <param name="settings">Pooler settings.</param>
        /// <returns>
        /// The newly created <see cref="ObjectPooler"/>.
        /// </returns>
        public static ObjectPooler CreatePooler(ObjectPoolerSettings settings)
        {
            if (objectPoolers == null)
            {
                objectPoolers = new List<ObjectPooler>();
            }

            var newPooler = new ObjectPooler(settings);
            
            objectPoolers.Add(newPooler);
            
            return newPooler;
        }

        /// <summary>
        /// Destroys the passed pooler.
        /// </summary>
        /// <param name="objectPooler">Pooler to destroy.</param>
        /// <exception cref="ArgumentNullException">Thrown when puller is null.</exception>
        public static void DestroyPooler(ObjectPooler objectPooler)
        {
            if (objectPooler == null)
            {
                throw new ArgumentNullException($"Trying to destroy a pooler that doesn't exist! Make sure something else hasn't destroyed it!");
            }
            
            objectPooler.Destroy();
        }

        /// <summary>
        /// Locates a pooler with the passed name.
        /// </summary>
        /// <param name="poolerName">Pooler name.</param>
        /// <returns>
        /// An instance of <see cref="ObjectPooler"/> if a pooler with queried name exists, null otherwise.
        /// </returns>
        public static ObjectPooler FindPoolerWithName(string poolerName)
        {
            foreach (ObjectPooler pooler in objectPoolers)
            {
                if (pooler.Name.Equals(poolerName))
                {
                    return pooler;
                }
            }

            Debug.LogError($"Failed to find pooler with name {poolerName}");
            return null;
        } 

        #endregion
    }
}