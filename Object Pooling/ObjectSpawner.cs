﻿using Sirenix.Utilities;
using Unidork.Attributes;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Unidork.ObjectPooling
{
    /// <summary>
    /// Spawns objects on request. Objects are acquired from an <see cref="objectPooler"/> that this component creates on start.
    /// </summary>
    public class ObjectSpawner : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Spawner's name.
        /// </summary>
        /// <value>
        /// Gets the value of the string field name.
        /// </value>
        public string Name => name;

        /// <summary>
        /// Can this spawner receive requests from other objects? We wait until the connected pooler creates
        /// its pool before allowing the first request.
        /// </summary>
        /// <value>
        /// Gets the value of the <see cref="ObjectPooler.FinishedCreatingPool"/> property on the connected object pooler.
        /// </value>
        public bool CanSpawnObjects => objectPooler != null && objectPooler.FinishedCreatingPool;

        #endregion
        
        #region Fields

        /// <summary>
        /// Arrray of all spawners currently present in the game.
        /// </summary>
        private static ObjectSpawner[] objectSpawners;

        /// <summary>
        /// Spawner's name.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Spawner's name.")]
        [SerializeField]
        private new string name = "NewSpawner";
        
        /// <summary>
        /// Settings to use when creating the pooler that will be tied to this spawner.
        /// </summary>
        [Tooltip("Settings to use when creating the pooler that will be tied to this spawner.")]
        [SerializeField]
        private ObjectPoolerSettings poolerSettings = null;

        /// <summary>
        /// Transform that serves as a holder for spawned objects in case no other transform is provided by calling scripts.
        /// </summary>
        [Tooltip("Transform that serves as a holder for spawned objects in case no other transform is provided by calling scripts.")]
        [SerializeField]
        private Transform defaultSpawnedObjectHolder = null;

        /// <summary>
        /// Object pooler that stores objects spawned by this spawner.
        /// </summary>
        private ObjectPooler objectPooler;
        
        #endregion

        #region Start
        private void Start() => objectPooler = ObjectPooler.CreatePooler(poolerSettings);

        #endregion

        #region Spawn

        /// <summary>
        /// Spawns a pooled object at the specified position and rotation.
        /// </summary>
        /// <param name="assetReferece">Asset reference.</param>
        /// <param name="position">Position.</param>
        /// <param name="rotation">Rotation.</param>
        /// <param name="parent">Transform to use as a parent for the spawned object.</param>
        /// <param name="autoActivate">Should the object be auto-activated upon spawning?</param>
        /// <returns>
        /// An instance of <see cref="IPooledObject"/> or null if no valid object was acquired from the connected pooler.
        /// </returns>
        public IPooledObject Spawn(AssetReferenceGameObject assetReferece, Vector3 position, Quaternion rotation,
                                   Transform parent = null, bool autoActivate = true)
        {
            IPooledObject spawnedObject = objectPooler.Get(assetReferece);

            if (spawnedObject == null)
            {
                return null;
            }

            if (parent == null)
            {
                parent = defaultSpawnedObjectHolder;
            }
            
            spawnedObject.SetUpTransform(parent, position, rotation);

            if (autoActivate)
            {
                spawnedObject.Activate();    
            }

            return spawnedObject;
        }

        /// <summary>
        /// Deactivates a pooled object and parents it to the pooled object holder.
        /// </summary>
        /// <param name="pooledObject">Pooled object to despawn.</param>
        public void Despawn(IPooledObject pooledObject)
        {
            pooledObject.Deactivate();
            pooledObject.SetParent(poolerSettings.PooledObjectHolder);
        }

        #endregion

        #region Static

        /// <summary>
        /// Gets a spawner with a passed name.
        /// </summary>
        /// <param name="spawnerName">Spawner name.</param>
        /// <returns>
        /// An instance of <see cref="ObjectSpawner"/> that matches the passed name, null otherwise.
        /// </returns>
        public static ObjectSpawner GetSpawnerWithName(string spawnerName)
        {
            if (objectSpawners.IsNullOrEmpty())
            {
                UpdateSpawnerArray();
            }

            foreach (ObjectSpawner objectSpawner in objectSpawners)
            {
                if (objectSpawner.Name == spawnerName)
                {
                    return objectSpawner;
                }
            }
            
            Debug.LogError($"Failed to find an object spawner with name {spawnerName}");
                
            return null;
        }

        /// <summary>
        /// Updates the static array of all spawners.
        /// </summary>
        private static void UpdateSpawnerArray() => objectSpawners = FindObjectsOfType<ObjectSpawner>();

        #endregion
    }
}