﻿using UnityEngine;

namespace Unidork.ObjectPooling
{
    /// <summary>
    /// Interface for objects that can be pooled by an <see cref="ObjectPooler"/>.
    /// </summary>
    public interface IPooledObject
    {
        bool IsActive { get; }

        void SetUpTransform(Transform parent, Vector3 position, Quaternion rotation);
        void SetParent(Transform parent);
        
        void Activate();
        void Deactivate();
        void Destroy();
    }
}