﻿namespace Unidork.Events
{
	using UnityEditor;
	using UnityEngine;

	[CustomEditor(typeof(GameEvent))]
	public class GameEventEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GameEvent @event = target as GameEvent;

			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("Raise"))
			{
				@event.Raise();
			}

			GUI.enabled = true;

			GUILayout.Label("DEBUG", EditorStyles.boldLabel);

			if (GUILayout.Button("Find All References"))
			{
				GameEventReferenceWindow.ShowWindow(@event);				
			}
		}
	}
}
