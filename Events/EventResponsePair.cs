﻿using UnityEngine;
using UnityEngine.Events;

namespace Unidork.Events
{
    /// <summary>
    /// Holds a pair of event and respective response.
    /// </summary>
    [System.Serializable]
    public class EventResponsePair
    {
        #region Properties

        /// <summary>
        /// <see cref="GameEvent"/> to register a response with.
        /// </summary>
        /// <value>
        /// Gets the value of the field @event.
        /// </value>
        public GameEvent Event => @event;

        /// <summary>
        /// Response to invoke when <see cref="GameEvent"/> is raised.
        /// </summary>
        /// <value>
        /// Gets the value of the field response.
        /// </value>
        public UnityEvent Response => response;

        #endregion

        #region Fields

        /// <summary>
        /// <see cref="GameEvent"/> to register a response with.
        /// </summary>
        [Tooltip("GameEvent to register a response with.")]
        [SerializeField]
        private GameEvent @event = null;

        /// <summary>
        /// Response to invoke when <see cref="GameEvent"/> is raised.
        /// </summary>
        [Tooltip("Response to invoke when GameEvent is raised.")]
        [SerializeField]
        private UnityEvent response = null;

        #endregion
    }
}

