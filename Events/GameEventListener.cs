﻿using UnityEngine;

namespace Unidork.Events
{
    /// <summary>
    /// Stores a list of events to listen to and respective responses.
    /// </summary>
    [System.Serializable]
#if UNITY_EDITOR
	[ExecuteAlways]
#endif
	public class GameEventListener : MonoBehaviour
    {
		#region Properties

		/// <summary>
		/// Array of event-response pairs belonging to this listener.
		/// </summary>
		/// <value>
		/// Gets the value of the field eventResponsePairs.
		/// </value>
		public EventResponsePair[] EventReponsePairs => (EventResponsePair[])eventResponsePairs.Clone();

		#endregion

		#region Fields

		/// <summary>
		/// Array of event-response pairs belonging to this listener.
		/// </summary>
		[Tooltip("Array of event-response pairs belonging to this listener.")]
        [SerializeField]
        private EventResponsePair[] eventResponsePairs = null;

        #endregion

        #region Enable/disable

        private void Awake() => OnEnable();

        private void OnEnable()
        {
            if (!enabled)
            {
                return;
            }

            if (eventResponsePairs == null)
            {
                return;
            }

            foreach (EventResponsePair e in eventResponsePairs)
            {
                if (e.Event == null || e.Event.EventListeners == null || e.Event.EventListeners.Contains(this))
                {
                    continue;
                }

                e.Event?.RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (eventResponsePairs == null)
            {
                return;
            }

			foreach (EventResponsePair e in eventResponsePairs)
            {
                e.Event?.UnregisterListener(this);
            }
        }

/*#if UNITY_EDITOR

		private void OnDestroy()
		{
			if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
			{
				if (GameEventListenerWindow.IsShowing)
				{
					UnityEditor.EditorWindow.GetWindow<GameEventListenerWindow>().RefreshListenerList();
				}
			}
		}

#endif*/

		#endregion

		/*#if UNITY_EDITOR
                private void OnValidate()
                {
                    if (Listeners == null)
                    {
                        return;
                    }                

                    if (!gameObject.activeInHierarchy)
                    {
                        return;
                    }                

                    if (EditorApplication.isPlayingOrWillChangePlaymode)
                    {
                        return;
                    }                

                    foreach (EventResponsePair e in Listeners)
                    {
                        e.Event?.RegisterListener(this);
                    }
                }
        #endif*/

		#region Event

		/// <summary>
		/// Invokes listeners if their event's name matches the name of the passed argument.
		/// </summary>
		/// <param name="e">Target event.</param>
		public void OnEventRaised(GameEvent e)
        {
            foreach (EventResponsePair eventResponsePair in eventResponsePairs)
            {
                if (e.Equals(eventResponsePair.Event))
                {

#if UNITY_EDITOR
                    if (eventResponsePair.Event.LogToConsole)
                    {
                        Debug.Log(string.Format("<b>{0}</b> is invoking <i>{1}</i> in response to <i>{2}</i> event", eventResponsePair.Response.GetPersistentTarget(0).name,
                                                                                                                    eventResponsePair.Response.GetPersistentMethodName(0),
                                                                                                                    eventResponsePair.Event.name));
                    }
#endif
                    eventResponsePair.Response.Invoke();
                }
            }
        }

        #endregion
    }
}
