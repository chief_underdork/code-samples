﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace Unidork.Events
{
    /// <summary>
    /// A scriptable object that holds a unique game event type.
    /// </summary>
    [CreateAssetMenu(fileName = "New Event", menuName = "Scriptable Objects/Events/Event", order = 2)]
    public class GameEvent : ScriptableObject
    {
        #region Properties

        public string Name => name;

        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        public List<GameEventListener> EventListeners { get => eventListeners; set => eventListeners = value; }

        /// <summary>
        /// When set to True, will log each invokation connected to this event.
        /// </summary>
        /// <value>
        /// Gets the value of the boolean field logToConsole.
        /// </value>
        public bool LogToConsole { get => logToConsole; set => logToConsole = value; }

        #endregion

        #region Fields

        /// <summary>
        /// When set to True, will log each invokation connected to this event.
        /// </summary>
        [Tooltip("When set to True, will log each invokation connected to this event.")]
        [SerializeField]
        private bool logToConsole = true;
        
#if dUI_MANAGER
        /// <summary>
        /// Should a Doozy UI event be raised as well>
        /// </summary>
        [Tooltip("Should a Doozy UI event be raised as well>")]
        [SerializeField]
        private bool raiseDoozyUIEvent = false;
#endif
        
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        [Tooltip("The list of listeners that this event will notify if it is raised.")]
        [SerializeField, ReadOnly]
        private List<GameEventListener> eventListeners;

        #endregion

        #region Event

        /// <summary>
        /// Raises the event, notifying all listeners. Raises an optional DoozyUI event if <paramref name="raiseDoozyUIEvent"/>
        /// is True.
        /// </summary>
        /// <param name="raiseDoozyUIEvent">Should a Doozy UI event be raised as well?</param>
        public void Raise()
        {
            for (int index = EventListeners.Count - 1; index >= 0; index--)
            {
                GameEventListener currentListener = eventListeners[index];
                
                if (currentListener == null)
                {
                    eventListeners.RemoveAt(index);
                    continue;
                }
                
                currentListener.OnEventRaised(this);
            }

#if dUI_MANAGER
            if (raiseDoozyUIEvent)
            {
                Doozy.Engine.Message.Send(new Doozy.Engine.GameEventMessage(name));
            }
#endif
        }

        #endregion

        #region Listeners

        /// <summary>
        /// Registers a new listeners.
        /// </summary>
        /// <param name="listener">Listener to register.</param>
        public void RegisterListener(GameEventListener listener)
        {
            if (EventListeners == null)
            {
                EventListeners = new List<GameEventListener>();
            }

            if (!EventListeners.Contains(listener))
            {
                EventListeners.Add(listener);
            }
        }

        /// <summary>
        /// Unregisters an existing listeners.
        /// </summary>
        /// <param name="listener">Listener to unregister.</param>
        public void UnregisterListener(GameEventListener listener)
        {
            if (EventListeners.Contains(listener))
            {
                EventListeners.Remove(listener);
            }
        }

        #endregion

        private void OnEnable() => EventListeners = new List<GameEventListener>();
    }
}
