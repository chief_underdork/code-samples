﻿using System.Collections;
using Cinemachine;
using Sirenix.OdinInspector;
using Unidork.Attributes;
using Unidork.Events;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Switches between Cinemachine's virtual cameras.
    /// </summary>
    public class VirtualCameraSwitcher : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that controls Cinemachine's virtual cameras.
        /// </summary>
        private CinemachineBrain cinemachineBrain = null;
        
        /// <summary>
        /// Virtual camera used when player is in the main menu.
        /// </summary>
        [Space, Title("CAMERAS", TitleAlignment = TitleAlignments.Centered, HorizontalLine = false), Space]
        [Tooltip("Virtual camera used when player is in the main menu.")]
        [SerializeField]
        private CinemachineVirtualCamera mainMenuCamera = null;
        
        /// <summary>
        /// Virtual camera used when player is in the mission selection view.
        /// </summary>
        [Tooltip("Virtual camera used when player is in the mission selection view.")]
        [SerializeField]
        private CinemachineVirtualCamera missionSelectionCamera = null;
        
        /// <summary>
        /// Virtual camera used as main gameplay camera.
        /// </summary>
        [Tooltip("Virtual camera used as main gameplay camera.")]
        [SerializeField]
        private CinemachineVirtualCamera mainGameplayCamera = null;

        /// <summary>
        /// Virtual camera used when character is using a jump pad.
        /// </summary>
        [Tooltip("Virtual camera used when character is using a jump pad.")]
        [SerializeField]
        private CinemachineVirtualCamera jumpPadCamera = null;

        /// <summary>
        /// Event to raise after transition to the mission view camera finishes.
        /// </summary>
        [Space, EventsHeader, Space]
        [Tooltip("Event to raise after transition to the mission view camera finishes.")]
        [SerializeField]
        private GameEvent onSwitchedToMissionViewCamera = null;

        #endregion

        #region Init

        private void Awake()
        {
#if UNITY_EDITOR

            // Doing this to eliminate index out of range exception when we're entering play mode
            // in the editor via any scene other than Loading screen due to Cinemachine brain not existing yet.
            try
            {
                cinemachineBrain = CinemachineCore.Instance.GetActiveBrain(0);
            }
            catch
            {
                // ignored
            }
#else
            cinemachineBrain = CinemachineCore.Instance.GetActiveBrain(0);
#endif

        }

        #endregion

        #region Constants

        /// <summary>
        /// Priority value to set on an active camera.
        /// </summary>
        private const int ActiveCameraPriority = 100;

        /// <summary>
        /// Priority value to set on an inactive camera.
        /// </summary>
        private const int InactiveCameraPriority = 1;

        #endregion

        #region Switch

        /// <summary>
        /// Makes the main menu camera the active virtual camera.
        /// </summary>
        [TriggeredByEvent("OnGameStateSwitchedToMainMenu")]
        public void SwitchToMainMenuCamera()
        {
            LowerPriorityOnActiveCamera();
            mainMenuCamera.Priority = ActiveCameraPriority;
        }

        /// <summary>
        /// Makes the mission selection camera the active virtual camera.
        /// </summary>
        [TriggeredByEvent("OnGameStateSwitchedToMissionSelection")]
        public void SwitchToMissionSelectionCamera()
        {
            LowerPriorityOnActiveCamera();
            missionSelectionCamera.Priority = ActiveCameraPriority;

            _ = StartCoroutine(RaiseEventAfterCameraBlendsFinishes(mainMenuCamera, onSwitchedToMissionViewCamera));
        }
        
        /// <summary>
        /// Makes the main gameplay camera the active virtual camera.
        /// </summary>
        [TriggeredByEvent("OnCharacterFinishedPadJump")]
        public void SwitchToGameplayCamera()
        {
            LowerPriorityOnActiveCamera();
            mainGameplayCamera.Priority = ActiveCameraPriority;
        }

        /// <summary>
        /// Makes the jump pad camera the active virtual camera.
        /// </summary>
        [TriggeredByEvent("OnCharacterCollidedWithJumpPad")]
        public void SwitchToJumpPadCamera()
        {
            LowerPriorityOnActiveCamera();
            jumpPadCamera.Priority = ActiveCameraPriority;
        }

        /// <summary>
        /// Lowes the priority on currently active Cinemachine virtual camera.
        /// </summary>
        private void LowerPriorityOnActiveCamera()
        {
            if (cinemachineBrain.ActiveVirtualCamera == null)
            {
                return;
            }
            
            cinemachineBrain.ActiveVirtualCamera.Priority = InactiveCameraPriority;
        }

        #endregion

        #region Events

        /// <summary>
        /// Raises an event after the specified Cinemachine's virtual camera finishes blending.
        /// </summary>
        /// <param name="camera">Virtual camera.</param>
        /// <param name="eventToRaise">Event to raise.</param>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator RaiseEventAfterCameraBlendsFinishes(CinemachineVirtualCamera camera, GameEvent eventToRaise)
        {
            yield return new WaitWhile(() => CinemachineCore.Instance.IsLive(camera));
            eventToRaise.Raise();
        }

        #endregion
    }
}