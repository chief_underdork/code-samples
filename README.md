**PLEASE NOTE THAT THESE CODE SAMPLES HAVE BEEN PLUCKED OUT OF THEIR NATURAL HABITAT FOR DEMONSTRATION PURPOSES AND WON'T ACTUALLY COMPILE DUE TO MISSING PARTS OF CODE.**

They DO work but you'll have to trust me on this;)

_Camera_ - a basic camera switcher for Cinemachine's virtual cameras.

_Events_ - a scriptable-object-based event system from Ryan Hipple's Unite talk but modified and expanded with stuff like event/listener locators etc.

_Extensions_ - various extensions methods for components/collections/whatnot.

_Grid Snapping_ - bunch of scripts that allow to snap objects to a grid covering a level chunk with some shortcuts for convenience.

_Move Action Controller_ - a controller that triggers/chains movement actions in a runner game.

_Object Pooling_ - a simple object pooling/spawning system with some utility methods.

_Scene Management_ - a basic implementation of scene loading/unloading via Addressables.

_State Machine_ - a simple scriptable-object-based finite state machine, expanding on the idea from the Unity tutorial on SO state machines.

_UI toolkit_ - a custom editor for a SO made using the UI Toolkit (formerly UI elements).
