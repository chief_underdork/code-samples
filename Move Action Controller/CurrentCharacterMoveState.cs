﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Unidork.Attributes;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AwesomeRunner
{
    /// <summary>
    /// Scriptable object that stores character's current move state which includes currently active action or actions.
    /// </summary>
    [CreateAssetMenu(fileName = "CurrentCharacterMoveState", menuName = "Character/Current Character Move State", order = 0)]
    public class CurrentCharacterMoveState : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Is the character currently running? True only when no other active actions are present.
        /// </summary>
        public bool IsRunning => activeActions.Count == 1;

        /// <summary>
        /// Does the set of currently active actions include jumping?
        /// </summary>
        public bool IsJumping => ActionOfTypeIsActive(CharacterMoveActionType.Jump);
        
        /// <summary>
        /// Does the set of currently active actions include strafing?
        /// </summary>
        public bool IsStrafing => ActionOfTypeIsActive(CharacterMoveActionType.Strafe);
        
        /// <summary>
        /// Does the set of currently active actions include sliding?
        /// </summary>
        public bool IsSliding => ActionOfTypeIsActive(CharacterMoveActionType.Slide);

        /// <summary>
        /// Does the set of currently active actions include diving?
        /// </summary>
        public bool IsDiving => ActionOfTypeIsActive(CharacterMoveActionType.Dive);

        #if UNITY_EDITOR

        [Space, DebugHeader, Space]
        [SerializeField, ReadOnly]
        private bool isRunning;
        
        [SerializeField, ReadOnly, UsedImplicitly]
        private bool isJumping;
        
        [SerializeField, ReadOnly, UsedImplicitly]
        private bool isStrafing;
        
        [SerializeField, ReadOnly, UsedImplicitly]
        private bool isSliding;

        [SerializeField, ReadOnly, UsedImplicitly]
        private bool isDiving;
        
        #endif
        
        #endregion

        #region Fields

        private readonly HashSet<CharacterMoveActionBase> activeActions = new HashSet<CharacterMoveActionBase>(); 

        #endregion

        #region Actions

        
        /// <summary>
        /// Adds an action to the set of active actions.
        /// </summary>
        /// <param name="actionToAdd">Action to add.</param>
        public void AddAction(CharacterMoveActionBase actionToAdd)
        {
            _ = activeActions.Add(actionToAdd);
            
#if UNITY_EDITOR
          UpdateDebugValues();
#endif
        }

        /// <summary>
        /// Removes an action from the set of active actions.
        /// </summary>
        /// <param name="actionToRemove">Action to remove.</param>
        public void RemoveAction(CharacterMoveActionBase actionToRemove)
        {
            _ = activeActions.Remove(actionToRemove);
#if UNITY_EDITOR
            UpdateDebugValues();
#endif
        }

        /// <summary>
        /// Checks whether action of specified type is currently active.
        /// </summary>
        /// <param name="actionType">Action type.</param>
        /// <returns>
        /// True if <see cref="activeActions"/> contain an action of passed type, False otherwise.
        /// </returns>
        private bool ActionOfTypeIsActive(CharacterMoveActionType actionType)
        {
            foreach (CharacterMoveActionBase activeAction in activeActions)
            {
                if (activeAction.Type == actionType)
                {
                    return true;
                }        
            }

            return false;
        }
        
        #if UNITY_EDITOR

        private void UpdateDebugValues()
        {
            isRunning = activeActions.Count == 1;

            isStrafing = !isRunning && ActionOfTypeIsActive(CharacterMoveActionType.Strafe);
            isJumping = !isRunning && ActionOfTypeIsActive(CharacterMoveActionType.Jump);
            isSliding = !isRunning && ActionOfTypeIsActive(CharacterMoveActionType.Slide);
            isDiving = !isRunning && ActionOfTypeIsActive(CharacterMoveActionType.Dive);
        }
        
        #endif

        #endregion
    }
}