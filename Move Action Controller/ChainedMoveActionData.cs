﻿namespace AwesomeRunner
{
    /// <summary>
    /// Stores data about a character move action that has been chained by the move controller.
    /// </summary>
    public class ChainedMoveActionData
    {
        #region Properties

        /// <summary>
        /// Character move action.
        /// </summary>
        public CharacterMoveActionBase Action { get; }
        
        /// <summary>
        /// Additional data required by the action.
        /// </summary>
        public object AdditionalData { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="action">Character move action.</param>
        /// <param name="additionalData">Additional data required by the action.</param>
        public ChainedMoveActionData(CharacterMoveActionBase action, object additionalData = null)
        {
            Action = action;
            AdditionalData = additionalData;
        }

        #endregion
    }
}