﻿namespace AwesomeRunner
{
    /// <summary>
    /// Direction of character's strafe.
    /// </summary>
    public enum CharacterStrafeDirection
    {
        None,
        Right,
        Left
    }
}