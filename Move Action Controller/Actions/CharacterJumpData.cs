﻿namespace AwesomeRunner
{
    /// <summary>
    /// Jump parameters passed to the <see cref="CharacterJumpAction"/> when it is initiated.
    /// </summary>
    public class CharacterJumpData
    {
        #region Fields

        /// <summary>
        /// Should fall phase be forced when the jump action is initiated?
        /// </summary>
        public readonly bool ForceFallPhase;

        /// <summary>
        /// Should the jump pad force multiplier be applied to the jump?
        /// </summary>
        public readonly bool ApplyJumpPadMultiplier;
        
        #endregion
        
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="forceFallPhase">Should fall phase be forced when the jump action is initiated?</param>
        /// <param name="applyJumpPadMultiplier">Should the jump pad force multiplier be applied to the jump?.</param>
        public CharacterJumpData(bool forceFallPhase, bool applyJumpPadMultiplier)
        {
            ForceFallPhase = forceFallPhase;
            ApplyJumpPadMultiplier = applyJumpPadMultiplier;
        }

        #endregion
    }
}