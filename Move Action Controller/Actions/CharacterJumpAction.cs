﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Unidork.Attributes;
using Unidork.Variables;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Performs the jump action of the playable character.
    /// </summary>
    public class CharacterJumpAction : CharacterMoveActionBase, IPausable
    {
        #region Properties

        /// <summary>
        /// Type of this move action.
        /// </summary>
        public override CharacterMoveActionType Type => CharacterMoveActionType.Jump;

        #endregion
        
        #region Fields
        
        /// <summary>
        /// Height of the player's jump.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Height of the player's jump.")]
        [SerializeField]
        private float jumpHeight = 2f;

        /// <summary>
        /// Time it takes for the character to reach max jump height.
        /// </summary>
        [Tooltip("Time it takes for the character to reach max jump height.")]
        [SerializeField]
        private float timeToJumpMaxHeight = 0.5f;

        /// <summary>
        /// Multiplier to apply to the jump force when the character uses a jump pad.
        /// </summary>
        [Tooltip("Multiplier to apply to the jump force when the character uses a jump pad.")]
        [SerializeField]
        private float jumpPadForceMultiplier = 3;
        
        /// <summary>
        /// Scriptable object that holds current grounded value.
        /// </summary>
        [Space, AssetsHeader, Space]
        [Tooltip("Scriptable object that holds current grounded value.")]
        [SerializeField]
        private BoolVariable isGrounded = null;
        
        /// <summary>
        /// Scriptable objects that holds the value of playable character's vertical speed.
        /// </summary>
        [Tooltip("Scriptable objects that holds the value of playable character's vertical speed.")]
        [SerializeField]
        private FloatVariable currentVerticalSpeed = null;

        /// <summary>
        /// Gravity to apply to the playable character.
        /// </summary>
        private float gravity;

        /// <summary>
        /// Value of character's vertical speed when a jump starts.
        /// </summary>
        private float jumpSpeed;

        /// <summary>
        /// Is the jump action currently blocked by a different move action?
        /// </summary>
        private bool isBlocked;

        /// <summary>
        /// Is the jump action currently paused?
        /// </summary>
        private bool isPaused;

        /// <summary>
        /// Method to call when jump is finished.
        /// </summary>
        private Action jumpCompletedCallback;
        
        #endregion

        #region Init

        private void Start()
        {
            gravity = -(2 * jumpHeight) / (timeToJumpMaxHeight * timeToJumpMaxHeight);
            jumpSpeed = Mathf.Abs(gravity * timeToJumpMaxHeight);

            enabled = false;
        }

        #endregion

        #region Jump

        /// <summary>
        /// Performs the jump action.
        /// </summary>
        /// <param name="slideCompletedCallback">Method to call when jump is finished.</param>
        /// <param name="args">Optional arguments.</param>
        public override void Perform(Action slideCompletedCallback, object args = null)
        {
            if (args != null)
            {
                var jumpData = (CharacterJumpData) args;
                currentVerticalSpeed.Value = jumpData.ForceFallPhase ? 0f :
                    jumpSpeed * (jumpData.ApplyJumpPadMultiplier ? jumpPadForceMultiplier : 1f);
            }
            else
            {
                currentVerticalSpeed.Value = jumpSpeed;
            }
            
            this.jumpCompletedCallback = slideCompletedCallback;
            
            isGrounded.Value = false;
            enabled = true;
        }

        private void Update()
        {
            if (isPaused)
            {
                return;
            }
            
            if (isGrounded.Value)
            {
                jumpCompletedCallback?.Invoke();
                enabled = false;
                return;
            }
            
            float verticalSpeed = currentVerticalSpeed.Value;

            transform.Translate(Vector3.up * verticalSpeed * Time.deltaTime);
            verticalSpeed += gravity * Time.deltaTime;

            currentVerticalSpeed.Value = verticalSpeed;
        }

        /// <summary>
        /// Stops the jump.
        /// </summary>
        public override void Stop()
        {
            enabled = false;
        }
        
        #endregion

        #region Block

        /// <summary>
        /// Blocks the jump action and pauses any active tweens.
        /// </summary>
        public override void Block()
        {
            isBlocked = true;
            Pause();
        }

        /// <summary>
        /// Unblocks the jump action and unpauses any active tweens.
        /// </summary>
        public override void Unblock()
        {
            isBlocked = false;
            Unpause();
        }

        #endregion

        #region Pause

        /// <summary>
        /// If character is jumping, pauses the jump tween.
        /// </summary>
        public void Pause()
        {
            if (isBlocked)
            {
                return;
            }

            isPaused = true;
        }

        /// <summary>
        /// If character is jumping, unpauses the jump tween.
        /// </summary>
        public void Unpause()
        {
            if (isBlocked)
            {
                return;
            }

            isPaused = false;
        }

        #endregion
    }
}