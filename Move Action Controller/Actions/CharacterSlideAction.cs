﻿using System;
using System.Collections;
using Unidork.Attributes;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Performs the slide action of the playable character.
    /// </summary>
    public class CharacterSlideAction : CharacterMoveActionBase, IPausable
    {
        #region Properties

        /// <summary>
        /// Type of this move action.
        /// </summary>
        public override CharacterMoveActionType Type => CharacterMoveActionType.Slide;

        #endregion
        
        #region Fields

        /// <summary>
        /// Duration of a slide.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Duration of a slide.")]
        [SerializeField]
        private float slideDuration = 0.5f;

        /// <summary>
        /// Method to call when slide is finished.
        /// </summary>
        private Action slideCompletedCallback;

        /// <summary>
        /// Is the slide action currently paused?
        /// </summary>
        private bool isPaused;

        /// <summary>
        /// Coroutine that handles the slide's timer.
        /// </summary>
        private Coroutine slideCoroutine;
        
        #endregion
        
        #region Slide

        /// <summary>
        /// Performs the slide action.
        /// </summary>
        /// <param name="slideCompletedCallback">Method to call when slide is finished.</param>
        /// <param name="args">Optional arguments.</param>
        public override void Perform(Action slideCompletedCallback, object args = null)
        {
            this.slideCompletedCallback = slideCompletedCallback;
            slideCoroutine = StartCoroutine(SlideCoroutine(slideDuration));
        }

        /// <summary>
        /// Stops the slide coroutine if it is running.
        /// </summary>
        public override void Stop()
        {
            if (slideCoroutine != null)
            {
                StopCoroutine(slideCoroutine);
            }
        }

        /// <summary>
        /// Coroutine that yields until the slide duration is reached and invokes the <see cref="slideCompletedCallback"/>.
        /// </summary>
        /// <param name="slideDuration"></param>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator SlideCoroutine(float slideDuration)
        {
            var timeSpentSliding = 0f;

            while (timeSpentSliding < slideDuration)
            {
                if (!isPaused)
                {
                    timeSpentSliding += Time.deltaTime;
                }

                yield return null;
            }
            
            slideCompletedCallback?.Invoke();
        }

        #endregion
        
        #region Pause

        /// <summary>
        /// Pauses this component.
        /// </summary>
        public void Pause() => isPaused = true;

        /// <summary>
        /// Unpauses this component.
        /// </summary>
        public void Unpause() => isPaused = false;

        #endregion
    }
}