﻿using System;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Base class for character move actions.
    /// </summary>
    public abstract class CharacterMoveActionBase : MonoBehaviour
    {
        #region Abstract

        public abstract CharacterMoveActionType Type { get; }
        public abstract void Perform(Action slideCompletedCallback, object args = null);
        public abstract void Stop();

        #endregion

        #region Virtual
        public virtual void Block() {}
        public virtual void Unblock() {}

        #endregion
    }
}