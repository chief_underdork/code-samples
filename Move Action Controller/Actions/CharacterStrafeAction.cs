﻿using System;
using DG.Tweening;
using Unidork.Attributes;
using Unidork.Tweens;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Performs the strafe action of the playable character.
    /// </summary>
    public class CharacterStrafeAction : CharacterMoveActionBase, IPausable
    {
        #region Properties
        /// <summary>
        /// Type of this move action.
        /// </summary>
        public override CharacterMoveActionType Type => CharacterMoveActionType.Strafe;

        #endregion
        
        #region Fields

        /// <summary>
        /// Duration of a strafe move.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Duration of a strafe move.")]
        [SerializeField]
        private float strafeDuration = 0.5f;

        /// <summary>
        /// Object that stores data about an easy function/curve to use by DoTween's SetEase() method.
        /// </summary>
        [Tooltip("Object that stores data about an easy function/curve to use by DoTween's SetEase() method.")]
        [SerializeField]
        private TweenData tweenData = new TweenData(); 
        
        /// <summary>
        /// Tween that animates character's x coordinate.
        /// </summary>
        private Tween strafeTween;

        #endregion

        #region Strafe

        /// <summary>
        /// Performs the strafe action.
        /// </summary>
        /// <param name="slideCompletedCallback">Method to call when strafe is finished.</param>
        /// <param name="args">Optional arguments.</param>
        public override void Perform(Action slideCompletedCallback, object args)
        {
            var strafeRight = (bool) args;

            Lane currentLane = LaneManager.SwitchLane(strafeRight, out bool laneWasSwitched);

            if (!laneWasSwitched)
            {
                slideCompletedCallback.Invoke();
                return;
            }

            strafeTween = transform.DOMoveX(currentLane.LaneXCoordinate, strafeDuration);

            strafeTween.OnComplete(() =>
            {
                slideCompletedCallback?.Invoke();
                strafeTween = null;
            });

            if (tweenData.UseCustomEaseCurve)
            {
                strafeTween.SetEase(tweenData.CustomEaseCurve);
            }
            else
            {
                strafeTween.SetEase(tweenData.EaseFunction);
            }
        }

        /// <summary>
        /// Stops the strafe by killing the strafe tween.
        /// </summary>
        public override void Stop() => strafeTween?.Kill();

        #endregion

        #region Pause

        /// <summary>
        /// If character is strafing, pauses the strafe tween.
        /// </summary>
        public void Pause() => strafeTween?.Pause();

        /// <summary>
        /// If character is strafing, unpauses the strafe tween.
        /// </summary>
        public void Unpause() => strafeTween?.Play();

        #endregion
    }
}