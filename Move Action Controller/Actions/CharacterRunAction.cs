﻿using System;

namespace AwesomeRunner
{
    /// <summary>
    /// Dummy class to use to specify that character is currently running in a straight line
    /// and not performing any other move actions.
    /// </summary>
    public class CharacterRunAction : CharacterMoveActionBase
    {
        public override CharacterMoveActionType Type => CharacterMoveActionType.Run;
        public override void Perform(Action slideCompletedCallback, object args = null)
        {
            throw new NotImplementedException("Should not be calling Perform on CharacterRunAction!");
        }

        public override void Stop()
        {
            throw new NotImplementedException("Should not be calling Perform on CharacterRunAction!");
        }
    }
}