﻿namespace AwesomeRunner
{
    /// <summary>
    /// Type of character's move action.
    /// </summary>
    public enum CharacterMoveActionType
    {
        Run,
        Strafe,
        Jump,
        Slide,
        Dive
    }
}