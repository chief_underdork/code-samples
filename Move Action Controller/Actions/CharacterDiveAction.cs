﻿using System;
using Unidork.Attributes;
using Unidork.Variables;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Performs the dive action of the playable character. A dive happens when a jump is interrupted by a slide action.
    /// Slide is chained to the dive automatically but can be canceled by a different action.
    /// </summary>
    public class CharacterDiveAction : CharacterMoveActionBase, IPausable
    {
        #region Properties

        /// <summary>
        /// Type of this move action.
        /// </summary>
        public override CharacterMoveActionType Type => CharacterMoveActionType.Dive;

        #endregion

        #region Fields

        /// <summary>
        /// Speed of the character's dive.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Speed of the character's dive.")]
        [SerializeField]
        private float diveSpeed = 30f;

        /// <summary>
        /// Acceleration of the character's dive.
        /// </summary>
        [Tooltip("Acceleration of the character's dive.")]
        [SerializeField]
        private float diveAcceleration = 15f;
        
        /// <summary>
        /// Scriptable objects that holds the value of playable character's vertical speed.
        /// </summary>
        [Space, AssetsHeader, Space]
        [Tooltip("Scriptable objects that holds the value of playable character's vertical speed.")]
        [SerializeField]
        private FloatVariable currentVerticalSpeed = null;
        
        /// <summary>
        /// Scriptable object that holds current grounded value.
        /// </summary>
        [Tooltip("Scriptable object that holds current grounded value.")]
        [SerializeField]
        private BoolVariable isGrounded = null;
        
        /// <summary>
        /// Is the jump action currently paused?
        /// </summary>
        private bool isPaused;
        
        /// <summary>
        /// Method to call when dive is finished.
        /// </summary>
        private Action diveCompletedCallback;

        #endregion

        #region Dive

        /// <summary>
        /// Performs the dive action.
        /// </summary>
        /// <param name="slideCompletedCallback"></param>
        /// <param name="args"></param>
        public override void Perform(Action slideCompletedCallback, object args = null)
        {
            this.diveCompletedCallback = slideCompletedCallback;

            currentVerticalSpeed.Value = 0f;
            enabled = true;
        }

        private void Update()
        {
            if (isPaused)
            {
                return;
            }

            if (isGrounded.Value)
            {
                currentVerticalSpeed.Value = 0f;
                diveCompletedCallback?.Invoke();
                enabled = false;
                return;
            }
            
            float verticalSpeed = currentVerticalSpeed.Value;
            verticalSpeed = Mathf.MoveTowards(verticalSpeed, -diveSpeed, diveAcceleration * Time.deltaTime);
            currentVerticalSpeed.Value = verticalSpeed;
            
            transform.Translate(Vector3.up * verticalSpeed * Time.deltaTime);
        }
        
        /// <summary>
        /// Stops the dive.
        /// </summary>
        public override void Stop()
        {
            
        }

        #endregion

        #region Pause

        /// <summary>
        /// If character is jumping, pauses the jump tween.
        /// </summary>
        public void Pause() => isPaused = true;

        /// <summary>
        /// If character is jumping, unpauses the jump tween.
        /// </summary>
        public void Unpause() => isPaused = false;

        #endregion
    }
}