﻿using Unidork.Attributes;
using Unidork.Extensions;
using Unidork.Variables;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Checks whether the player is currently colliding with ground or ramps.
    /// </summary>
    public class CharacterGroundChecker : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that handles movement of the playable character based on their current state, environment, vehicle etc. 
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Component that handles movement of the playable character based on their current state, environment, vehicle etc.")] 
        [SerializeField]
        private CharacterMoveActionController characterMoveActionController = null;
        
        /// <summary>
        /// Scriptable object that holds current grounded value.
        /// </summary>
        [Space, AssetsHeader, Space]
        [Tooltip("Scriptable object that holds current grounded value.")]
        [SerializeField]
        private BoolVariable isGrounded = null;

        /// <summary>
        /// Scriptable object that stores the ground object the character is currently moving on, if any.
        /// </summary>
        [Tooltip("Scriptable object that stores the ground object the character is currently moving on, if any.")]
        [SerializeField]
        private CurrentGroundObject currentGroundObject = null;

        /// <summary>
        /// Scriptable object that holds the index of the current vertical layer of the level the player is on.
        /// </summary>
        [Tooltip("Scriptable object that holds the index of the current vertical layer of the level the player is on.")]
        [SerializeField]
        private IntVariable currentVerticalLayerIndex = null;
        
        /// <summary>
        /// Scriptable objects that holds the value of playable character's vertical speed.
        /// </summary>
        [Tooltip("Scriptable objects that holds the value of playable character's vertical speed.")]
        [SerializeField]
        private FloatVariable currentVerticalSpeed = null;

        #endregion
        
        #region Fields

        /// <summary>
        /// Layer mask to use when detecting collisions with the ground.
        /// </summary>
        [Space, SettingsHeader, Space]
        [Tooltip("Layer mask to use when detecting collisions with the ground.")]
        [SerializeField]
        private LayerMask groundLayerMask = default;
        
        /// <summary>
        /// Layer mask to use when detecting collisions with ramps.
        /// </summary>
        [Tooltip("Layer mask to use when detecting collisions with ramps.")]
        [SerializeField]
        private LayerMask rampLayerMask = default;

        /// <summary>
        /// Character's main collider.
        /// </summary>
        [ComponentsHeader]
        [Tooltip("Character's main collider.")]
        [SerializeField]
        private BoxCollider characterCollider = null;

        /// <summary>
        /// Transforms to use as raycast origins. 
        /// </summary>
        [Tooltip("Transforms to use as raycast origins.")]
        [SerializeField]
        private Transform[] raycastOrigins = null;

        /// <summary>
        /// Length of raycasts.
        /// </summary>
        private float raycastLength;

        /// <summary>
        /// Half size of the character's collider.
        /// </summary>
        private Vector3 characterColliderExtents;
        
        /// <summary>
        /// Collider array to use for non-alloc box overlap checks.
        /// </summary>
        private Collider[] hitColliders = new Collider[1];

        /// <summary>
        /// Is this component currently paused?
        /// </summary>
        private bool isPaused;
        
        #endregion

        #region Init

        private void Start()
        {
            var characterColliderBounds = characterCollider.bounds;

            characterColliderExtents = characterColliderBounds.extents;
            
            const float raycastSafetyDistance = 0.01f;
            raycastLength = characterColliderBounds.size.y + raycastSafetyDistance;
        }

        #endregion

        #region Checks

        private void Update()
        {
            if (isPaused)
            {
                return;
            }

            bool wasGroundedOnPreviousFrame = isGrounded.Value;
            
            isGrounded.Value = false;
            
            if (currentVerticalSpeed.Value > 0f)
            {
                SetCurrentGround(null);
                return;
            }
            
            SetYPositionOnRamp();
            
            if (isGrounded.Value)
            {
                return;
            }
            
            PerformGroundCheck();

            if (!isGrounded.Value && wasGroundedOnPreviousFrame)
            {
                characterMoveActionController.PerformJumpFromFallPhase();
            }
        }

        #endregion

        #region Ramps

        /// <summary>
        /// Sets the Y coordinate of the character on a ramp they're colliding with.
        /// </summary>
        private void SetYPositionOnRamp()
        {
            foreach (Transform raycastOrigin in raycastOrigins)
            {
                if (!Physics.Raycast(raycastOrigin.position, Vector3.down, out RaycastHit hit, raycastLength,
                    rampLayerMask, QueryTriggerInteraction.Collide))
                {
                    continue;
                }

                float targetY = hit.point.y;

                var characterTransform = transform;
                Vector3 newPosition = characterTransform.position;
                newPosition.y = targetY;

                characterTransform.position = newPosition;

                SetCurrentGround(hit.collider);
                break;
            }
        }

        #endregion

        #region Ground

        private void PerformGroundCheck()
        {
            const float groundCheckOffset = 0.01f;
            
            Vector3 boxOrigin = characterCollider.bounds.center + Vector3.down * groundCheckOffset;

            int numberOfHitColliders = Physics.OverlapBoxNonAlloc(boxOrigin, characterColliderExtents, hitColliders,
                                                                  Quaternion.identity, groundLayerMask);

            if (numberOfHitColliders > 0)
            {
                Transform characterTransform = transform;
                Vector3 groundedPosition = characterTransform.position;
                groundedPosition.y = hitColliders[0].bounds.max.y;

                characterTransform.position = groundedPosition;
                
                Collider groundCollider = hitColliders[0];
                
                SetCurrentGround(groundCollider);
            }
            else
            {
                SetCurrentGround(null);
            }
        }

        /// <summary>
        /// Sets the value of the SO that holds current ground object and the SO that holds current grounded flag.
        /// </summary>
        /// <param name="groundCollider">Collider of the ground object.</param>
        private void SetCurrentGround(Collider groundCollider)
        {
            var grounded = false;
            Ground currentGround = null;

            if (groundCollider != null)
            {
                grounded = true;
                currentGround = groundCollider.GetComponentNonAlloc<Ground>();
            }
            
            isGrounded.Value = grounded;
            currentGroundObject.Value = currentGround;

            if (currentGround != null)
            {
                currentVerticalLayerIndex.Value = currentGround.VerticalLayerIndex;
            }
        }

        #endregion

        #region Pause

        /// <summary>
        /// Pauses this component.
        /// </summary>
        public void Pause() => isPaused = true;

        /// <summary>
        /// Unpauses this component.
        /// </summary>
        public void Unpause() => isPaused = false;

        #endregion
    }
}