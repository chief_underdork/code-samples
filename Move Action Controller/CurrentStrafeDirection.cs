﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Scriptable object that stores character's current strafe direction.
    /// </summary>
    [CreateAssetMenu(fileName = "CurrentStrafeDirection", menuName = "Character/Current Strafe Direction", order = 1)]
    public class CurrentStrafeDirection : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Current strafe direction value.
        /// </summary>
        [ShowInInspector, ReadOnly]
        public CharacterStrafeDirection Value { get; set; } = CharacterStrafeDirection.None;

        #endregion
    }
}

