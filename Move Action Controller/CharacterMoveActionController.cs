﻿using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Unidork.Attributes;
using Unidork.Events;
using Unidork.Variables;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Handles movement of the playable character based on their current state, environment, vehicle etc. 
    /// </summary>
    public class CharacterMoveActionController : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds the move input vector in the editor.
        /// </summary>
        [Space, AssetsHeader, Space]
        [Tooltip("Scriptable object that holds the move input vector in the editor.")]
        [SerializeField]
        private Vector2Variable moveInput = null;

        /// <summary>
        /// Scriptable object that stores character's current move state which includes currently active action or actions.
        /// </summary>
        [Tooltip("Scriptable object that stores character's current move state which includes currently active action or actions.")]
        [SerializeField] 
        private CurrentCharacterMoveState currentCharacterMoveState = null;

        /// <summary>
        /// Scriptable object that stores character's current strafe direction.
        /// </summary>
        [Tooltip("Scriptable object that stores character's current strafe direction.")]
        [SerializeField]
        private CurrentStrafeDirection currentStrafeDirection = null;
        
        /// <summary>
        /// Scriptable object that holds the "character is using jump pad" flag.
        /// </summary>
        [Tooltip("Scriptable object that holds the \"character is using jump pad\" flag.")]
        [SerializeField]
        private BoolVariable characterIsUsingJumpPad = null;

        /// <summary>
        /// Scriptable object that holds the can strafe flag for the playable character.
        /// </summary>
        [Tooltip("Scriptable object that holds the can strafe flag for the playable character.")]
        [SerializeField]
        private BoolVariable canStrafe = null;
        
        /// <summary>
        /// Scriptable object that holds the can jump flag for the playable character.
        /// </summary>
        [Tooltip("Scriptable object that holds the can jump flag for the playable character.")]
        [SerializeField]
        private BoolVariable canJump = null;
        
        /// <summary>
        /// Scriptable object that holds the can slide flag for the playable character.
        /// </summary>
        [Tooltip("Scriptable object that holds the can slide flag for the playable character.")]
        [SerializeField]
        private BoolVariable canSlide = null;

        /// <summary>
        /// Scriptable object that stores the index of the lane playable character is in.
        /// </summary>
        [Tooltip("Scriptable object that stores the index of the lane playable character is in.")] 
        [SerializeField]
        private IntVariable currentLaneIndex = null;
        
        /// <summary>
        /// Scriptable object that stores the index of the lane that the character is forced to strafe to.
        /// </summary>
        [Tooltip("Scriptable object that stores the index of the lane that the character is forced to strafe to.")]
        [SerializeField]
        private IntVariable forcedStrafeLaneIndex = null;

        /// <summary>
        /// Component that controls character's animator.
        /// </summary>
        [Space, ComponentsHeader, Space]
        [Tooltip("Component that controls character's animator.")]
        [SerializeField]
        private CharacterAnimationController characterAnimationController = null;
        
        /// <summary>
        /// Component that stores a dummy run action that is used when the character is not performing any other actions.
        /// </summary>
        [Space, Title("ACTIONS", TitleAlignment = TitleAlignments.Centered, HorizontalLine = false), Space] 
        [Tooltip("Component that stores a dummy run action that is used when the character is not performing any other actions.")]
        [SerializeField]
        private CharacterMoveActionBase runAction = null;
        
        /// <summary>
        /// Component that performs the strafe action of the playable character.
        /// </summary>
        [Tooltip("Component that performs the strafe action of the playable character.")]
        [SerializeField]
        private CharacterMoveActionBase strafeAction = null;

        /// <summary>
        /// Component that performs the jump action of the playable character.
        /// </summary>
        [Tooltip("Component that performs the jump action of the playable character.")]
        [SerializeField]
        private CharacterMoveActionBase jumpAction = null;

        /// <summary>
        /// Component that performs the slide action of the playable character.
        /// </summary>
        [Tooltip("Component that performs the slide action of the playable character.")]
        [SerializeField]
        private CharacterMoveActionBase slideAction = null;

        /// <summary>
        /// Component that performs the dive action of the playable character.
        /// </summary>
        [Tooltip("Component that performs the dive action of the playable character.")]
        [SerializeField]
        private CharacterDiveAction diveAction = null;
        
        /// <summary>
        /// Event that is raised when character finishes a jump that was started by a jump pad.
        /// </summary>
        [Space, EventsHeader, Space]
        [Tooltip("Event that is raised when character finishes a jump that was started by a jump pad.")]
        [SerializeField]
        private GameEvent onCharacterFinishedPadJump = null;

        /// <summary>
        /// Data about an action that will be performed immediately after current move action is finished.
        /// </summary>
        private ChainedMoveActionData chainedAction;

        /// <summary>
        /// Is input currently locked for this component?
        /// </summary>
        private bool inputLocked;

        #endregion

        #region Init

        private void Start() => currentCharacterMoveState.AddAction(runAction);

        #endregion

        #region Lock

        /// <summary>
        /// Locks input for this component.
        /// </summary>
        [TriggeredByEvent("OnLockMoveInput")]
        public void LockInput() => inputLocked = true;

        /// <summary>
        /// Unlocks input for this component.
        /// </summary>
        [TriggeredByEvent("OnUnlockMoveInput")]
        public void UnlockInput() => inputLocked = false;

        #endregion

        #region Input

        /// <summary>
        /// Processes move input vector, calculates swipe direction based on it and
        /// calls a move action method that corresponds with the swipe direction.
        /// </summary>
        public void HandleMoveInput()
        {
            if (inputLocked)
            {
                return;
            }
            
            SwipeDirection swipeDirection = CalculateSwipeDirection(moveInput.Value);

            switch (swipeDirection)
            {
                case SwipeDirection.Right:
                    PerformStrafe(moveRight: true);
                    break;
                case SwipeDirection.Left:
                    PerformStrafe(moveRight: false);
                    break;
                case SwipeDirection.Up:
                    PerformJump();
                    break;
                case SwipeDirection.Down:
                    PerformSlide();
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unlandled swipe direction: {swipeDirection}");
            }
        }

        /// <summary>
        /// Calculates swipe direction from a Vector2 that represents composite 2D vector input.
        /// </summary>
        /// <param name="input">Input vector.</param>
        /// <returns>
        /// A <see cref="SwipeDirection"/> that corresponds with the vector.
        /// </returns>
        private SwipeDirection CalculateSwipeDirection(Vector2 input)
        {
            if (input.x != 0)
            {
                return input.x < 0 ? SwipeDirection.Left : SwipeDirection.Right;
            }

            return input.y < 0 ? SwipeDirection.Down : SwipeDirection.Up;
        }

        #endregion
        
        #region Stop

        /// <summary>
        /// Stops all movement actions that are currently active.
        /// </summary>
        public void StopAllMoveActions()
        {
            if (currentCharacterMoveState.IsStrafing)
            {
                StopAction(strafeAction);
            }

            if (currentCharacterMoveState.IsJumping)
            {
                StopAction(jumpAction);
            }

            if (currentCharacterMoveState.IsSliding)
            {
                StopAction(slideAction);
            }

            chainedAction = null;
        }

        /// <summary>
        /// Stops a move action and removes it from <see cref="currentCharacterMoveState"/>.
        /// </summary>
        /// <param name="moveAction">Move action to stop.</param>
        private void StopAction(CharacterMoveActionBase moveAction)
        {
            moveAction.Stop();
            currentCharacterMoveState.RemoveAction(moveAction);
        }
        
        #endregion
        
        #region Strafe

        /// <summary>
        /// Performs a strafe action away from the obstacle that dazed the character while in a previous strafe.
        /// </summary>
        [TriggeredByEvent("OnCharacterBecameDazed")]
        public void PerformStrafeWhenDazed()
        {
            bool moveRight = currentStrafeDirection.Value == CharacterStrafeDirection.Left;
            PerformStrafe(moveRight);
        }

        [TriggeredByEvent("OnCharacterForcedToStrafe")]
        public void PerformForcedStrafe()
        {
            int forcedLaneIndex = forcedStrafeLaneIndex.Value;
            int laneIndex = this.currentLaneIndex.Value;
            CharacterStrafeDirection strafeDirection = GetStrafeDirection(laneIndex, forcedLaneIndex);

            switch (strafeDirection)
            {
                case CharacterStrafeDirection.None:
                    return;
                case CharacterStrafeDirection.Right:
                    ForceStrafeRight(Mathf.Abs(forcedLaneIndex - laneIndex));
                    break;
                case CharacterStrafeDirection.Left:
                    ForceStrafeLeft(Mathf.Abs(forcedLaneIndex - laneIndex));
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unhandled strafe direction: {strafeDirection}");
            } 
        }

        /// <summary>
        /// Performs the strafe action unless the character is already strafing.
        /// </summary>
        /// <param name="moveRight"></param>
        public void PerformStrafe(bool moveRight)
        {
            if (!canStrafe.Value)
            {
                return;
            }
            
            if (currentCharacterMoveState.IsDiving)
            {
                chainedAction = new ChainedMoveActionData(strafeAction, moveRight);
                return;
            }
            
            if (currentCharacterMoveState.IsStrafing)
            {
                if (currentStrafeDirection.Value == CharacterStrafeDirection.Right && moveRight ||
                    currentStrafeDirection.Value == CharacterStrafeDirection.Left && !moveRight)
                {
                    chainedAction = new ChainedMoveActionData(strafeAction, moveRight);
                    return;
                }
                
                StopAction(strafeAction);
            }

            currentStrafeDirection.Value = moveRight ? CharacterStrafeDirection.Right : CharacterStrafeDirection.Left;
            
            strafeAction.Perform(OnStrafePerformed, moveRight);
            currentCharacterMoveState.AddAction(strafeAction);
        }

        /// <summary>
        /// Callback for when a strafe finishes.
        /// </summary>
        private void OnStrafePerformed()
        {
            currentCharacterMoveState.RemoveAction(strafeAction);

            currentStrafeDirection.Value = CharacterStrafeDirection.None;
            
            PerformChainedAction();
        }

        /// <summary>
        /// Gets the strafe direction required to reach the lane with the passed index.
        /// </summary>
        /// <param name="laneIndex">Index of the lane character is currently in.</param>
        /// <param name="strafeLaneIndex">Index of the lane to strafe to.</param>
        /// <returns>
        /// <see cref="CharacterStrafeDirection.None"/> if current lane index matches the target lane index,
        /// <see cref="CharacterStrafeDirection.Right"/> or <see cref="CharacterStrafeDirection.Left"/> otherwise. 
        /// </returns>
        private CharacterStrafeDirection GetStrafeDirection(int laneIndex, int strafeLaneIndex)
        {
            if (laneIndex == strafeLaneIndex)
            {
                return CharacterStrafeDirection.None;
            }

            return strafeLaneIndex > laneIndex ? CharacterStrafeDirection.Right : CharacterStrafeDirection.Left;
        }
        
        /// <summary>
        /// Forces the specified number of strafes in the right direction.
        /// </summary>
        /// <param name="numberOfStrafes">Number of strafes.</param>
        private void ForceStrafeRight(int numberOfStrafes)
        {
            PerformStrafe(moveRight: true);
            
            if (numberOfStrafes == 2)
            {
                PerformStrafe(moveRight: true);
            }
        }

        /// <summary>
        /// Forces the specified number of strafes in the left direction.
        /// </summary>
        /// <param name="numberOfStrafes">Number of strafes.</param>
        private void ForceStrafeLeft(int numberOfStrafes)
        {
            PerformStrafe(moveRight: false);
            
            if (numberOfStrafes == 2)
            {
                PerformStrafe(moveRight: false);
            }
        }
        
        #endregion

        #region Jump

        /// <summary>
        /// Performs the jump action but forces it to start in the fall phase.
        /// </summary>
        public void PerformJumpFromFallPhase()
        {
            jumpAction.Perform(() => OnJumpFinished(jumpPadUsed:false), new CharacterJumpData(forceFallPhase: true, applyJumpPadMultiplier:false));
            currentCharacterMoveState.AddAction(jumpAction);
        }
        
        /// <summary>
        /// Performs the jump action unless the character is currently performing a move action.
        /// </summary>
        private void PerformJump()
        {
            if (!canJump.Value)
            {
                return;
            }
            
            if (currentCharacterMoveState.IsJumping || currentCharacterMoveState.IsDiving)
            {
                chainedAction = new ChainedMoveActionData(jumpAction);
                return;
            }
            
            if (currentCharacterMoveState.IsSliding)
            {
                StopAction(slideAction);
            }

            jumpAction.Perform(() => OnJumpFinished(jumpPadUsed:false));
            currentCharacterMoveState.AddAction(jumpAction);
            characterAnimationController.PlayJumpAnimation();
        }

        /// <summary>
        /// Performs the jump action when character collides with a jump pad. Applies jump pad multiplier.
        /// Stops all current move actions.
        /// </summary>
        [TriggeredByEvent("OnPlayerCollidedWithJumpPad")]
        public void PerformJumpOnJumpPad()
        {
            StopAllMoveActions();

            jumpAction.Perform(() => OnJumpFinished(jumpPadUsed:true), new CharacterJumpData(forceFallPhase:false, applyJumpPadMultiplier:true));
            currentCharacterMoveState.AddAction(jumpAction);

            characterIsUsingJumpPad.Value = true;
        }

        /// <summary>
        /// Callback for when a jump finishes.
        /// </summary>
        private void OnJumpFinished(bool jumpPadUsed)
        {
            if (jumpPadUsed)
            {
                onCharacterFinishedPadJump.Raise();
                characterIsUsingJumpPad.Value = false;
            }
            
            currentCharacterMoveState.RemoveAction(jumpAction);
            PerformChainedAction();
        }

        #endregion

        #region Slide

        /// <summary>
        /// Performs the slide action unless the character is currently performing a move action.
        /// </summary>
        private void PerformSlide()
        {
            if (!canSlide.Value)
            {
                return;
            }
            
            if (currentCharacterMoveState.IsJumping)
            {
                if (characterIsUsingJumpPad.Value)
                {
                    chainedAction = new ChainedMoveActionData(slideAction);
                    return;
                }

                StopAction(jumpAction);
                PerformDive();
                return;
            }

            if (currentCharacterMoveState.IsDiving)
            {
                chainedAction = new ChainedMoveActionData(slideAction);
                return;
            }
            
            if (!currentCharacterMoveState.IsRunning)
            {
                chainedAction = new ChainedMoveActionData(slideAction);
                return;
            }
            
            slideAction.Perform(OnSlideFinished);
            currentCharacterMoveState.AddAction(slideAction);
            characterAnimationController.PlaySlideAnimation();
        }

        /// <summary>
        /// Callback for when a slide finishes.
        /// </summary>
        private void OnSlideFinished()
        {
            currentCharacterMoveState.RemoveAction(slideAction);
            PerformChainedAction();
        }

        #endregion

        #region Dive

        /// <summary>
        /// Performs a dive action and chains a following slide.
        /// </summary>
        private void PerformDive()
        {
            diveAction.Perform(OnDiveFinished);
            currentCharacterMoveState.AddAction(diveAction);
            chainedAction = new ChainedMoveActionData(slideAction);
            
            //TODO: Remove this temporary solution after real animations are added
            transform.GetChild(0).localRotation = Quaternion.identity;
            transform.GetChild(0).localPosition = Vector3.zero;
            
            characterAnimationController.PlayDiveAnimation();
        }

        /// <summary>
        /// Callback for when a dive finishes.
        /// </summary>
        private void OnDiveFinished()
        {
            currentCharacterMoveState.RemoveAction(diveAction);
            PerformChainedAction();
        }

        #endregion

        #region Chained action

        /// <summary>
        /// Performs action stored in <see cref="chainedAction"/>, if any.
        /// </summary>
        private void PerformChainedAction()
        {
            if (chainedAction == null)
            {
                return;
            }

            switch (chainedAction.Action.Type)
            {
                case CharacterMoveActionType.Strafe:
                    PerformStrafe((bool)chainedAction.AdditionalData);
                    break;
                case CharacterMoveActionType.Jump:
                    PerformJump();
                    break;
                case CharacterMoveActionType.Slide:
                    PerformSlide();
                   break;
                default:
                    throw new ArgumentOutOfRangeException($"Unhandled move action type: {chainedAction.Action.Type}");
            }

            chainedAction = null;
        }

        #endregion
    }
}