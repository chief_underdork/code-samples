﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using Unidork.Attributes;
using Unidork.Variables;
using UnityEngine;

namespace AwesomeRunner
{
    /// <summary>
    /// Blocks specific move actions when playable character enters the associated collider
    /// and unblocks them after character leaves.
    /// </summary>
    public class CharacterMoveActionBlocker : MonoBehaviour, ICollidable
    {
        #region Properties

        /// <summary>
        /// Type of this collidable object.
        /// </summary>
        public CollidableType CollidableType => CollidableType.Trigger;

        #endregion
        
        #region Fields

        /// <summary>
        /// Types of move actions that this blocker blocks.
        /// </summary>
        [Space, SettingsHeader, Space]
        [ValueDropdown("GetTypesToBlock")]
        [SerializeField]
        private CharacterMoveActionType[] actionTypesToBlock = null;

        /// <summary>
        /// If this component blocks strafes, use this toggle to also force the character to strafe to a certain lane before blocking.
        /// </summary>
        [ShowIf("ContainsStrafeAction")]
        [Tooltip("If this component blocks strafes, use this toggle to also force the character to strafe to a certain lane before blocking.")]
        [SerializeField]
        private bool forceCharacterToLane = false;

        /// <summary>
        /// Lane index to force if Force Character To Lane is set to True.
        /// </summary>
        [ShowIf("@this.forceCharacterToLane")]
        [Tooltip("Lane index to force if Force Character To Lane is set to True.")]
        [ValueDropdown("textureIndices")]
        [SerializeField]
        private int laneIndexToForce = 0;
        
        /// <summary>
        /// Scriptable object that holds the can strafe flag for the playable character.
        /// </summary>
        [ShowIf("ContainsStrafeAction")]
        [Tooltip("Scriptable object that holds the can strafe flag for the playable character.")]
        [SerializeField]
        private BoolVariable canStrafe = null;
        
        /// <summary>
        /// Scriptable object that holds the can jump flag for the playable character.
        /// </summary>
        [ShowIf("ContainsJumpAction")]
        [Tooltip("Scriptable object that holds the can jump flag for the playable character.")]
        [SerializeField]
        private BoolVariable canJump = null;
        
        /// <summary>
        /// Scriptable object that holds the can slide flag for the playable character.
        /// </summary>
        [ShowIf("ContainsSlideAction")]
        [Tooltip("Scriptable object that holds the can slide flag for the playable character.")]
        [SerializeField]
        private BoolVariable canSlide = null;

        #region Odin

#if UNITY_EDITOR

        private bool BlocksStrafes
        {
            get
            {
                if (actionTypesToBlock.IsNullOrEmpty())
                {
                    return false;
                }

                foreach (CharacterMoveActionType actionTypeToBlock in actionTypesToBlock)
                {
                    if (actionTypeToBlock == CharacterMoveActionType.Strafe)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
        
        [Space, DebugHeader, Space]
        [SerializeField]
        private bool drawCubeGizmo = true;
        
        private IEnumerable<CharacterMoveActionType> GetTypesToBlock()
        {
            return new CharacterMoveActionType[]
            {
                CharacterMoveActionType.Strafe,
                CharacterMoveActionType.Jump,
                CharacterMoveActionType.Slide
            };
        }

        private bool ContainsJumpAction => ContainsActionOfType(CharacterMoveActionType.Jump);
        private bool ContainsSlideAction => ContainsActionOfType(CharacterMoveActionType.Slide);
        private bool ContainsStrafeAction => ContainsActionOfType(CharacterMoveActionType.Strafe);
        private bool ContainsActionOfType(CharacterMoveActionType actionType)
        {
            foreach (CharacterMoveActionType actionTypeToBlock in actionTypesToBlock)
            {
                if (actionTypeToBlock == actionType)
                {
                    return true;
                }
            }

            return false;
        }

        private static int[] textureIndices = new int[] {-1, 0, 1};
#endif       
        
        #endregion

        #endregion
        
        #region Block

        public void OnCollidedWithCharacter()
        {
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }
            
            SetValueOnMoveActionVariables(valueToSet: false);
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }
            
            SetValueOnMoveActionVariables(valueToSet: true);
        }

        /// <summary>
        /// Sets the move action variable value to the passed value.
        /// </summary>
        /// <param name="valueToSet">Value to set.</param>
        private void SetValueOnMoveActionVariables(bool valueToSet)
        {
            foreach (CharacterMoveActionType actionTypeToBlock in actionTypesToBlock)
            {
                if (actionTypeToBlock == CharacterMoveActionType.Strafe && valueToSet)
                {
                    
                }
                
                SetValueOnMoveActionVariable(GetBoolVariableForMoveActionType(actionTypeToBlock), valueToSet);
            }
        }

        /// <summary>
        /// Gets a BoolVariable that corresponds with a passed move action type.
        /// </summary>
        /// <param name="actionType">Character move action type.</param>
        /// <returns>
        /// A BoolVariable scriptable object whose type matches <see cref="actionType"/>.
        /// </returns>
        private BoolVariable GetBoolVariableForMoveActionType(CharacterMoveActionType actionType)
        {
            switch (actionType)
            {
                case CharacterMoveActionType.Strafe:
                    return canStrafe;
                case CharacterMoveActionType.Jump:
                    return canJump;
                case CharacterMoveActionType.Slide:
                    return canSlide;
                default:
                    throw new ArgumentOutOfRangeException(nameof(actionType), actionType, null);
            }
        }

        /// <summary>
        /// Sets the value of the passed BoolVariable to the specified value.
        /// </summary>
        /// <param name="moveActionVariable">Scriptable object that stores a Boolean flag for a move action type.</param>
        /// <param name="value">Value to set.</param>
        private void SetValueOnMoveActionVariable(BoolVariable moveActionVariable, bool value)
        {
            moveActionVariable.Value = value;
        }

        #endregion
        
#if UNITY_EDITOR
        
        #region Gizmos

        private void OnDrawGizmos()
        {
            if (!drawCubeGizmo)
            {
                return;
            }
            
            Bounds bounds = GetComponent<BoxCollider>().bounds;

            Color oldColor = Gizmos.color;

            Gizmos.color = new Color32(178, 34, 34, 127);

            Gizmos.DrawCube(bounds.center, bounds.size);

            Gizmos.color = oldColor;
        }

        #endregion
#endif
    }
}